<style type="text/css">
  .label-danger {
      background-color: #db3325;
  }
  @page { size: landscape; }
</style>
<div class="span12">
  <!-- /widget -->
          <div class="widget widget-table action-table" id="search-bar">
            <!-- /widget-header -->
            <div class="widget-content">
              <br>
              <form class="form-inline" action="<?php echo site_url(); ?>Reports" method="POST">
                  &nbsp;&nbsp;
                  <label for="type">EQ Type</label>
                      <select class="form-control" id="type" name="type" style="width: 100px;">
                         <option value="All">All</option>
                         <?php foreach ($eq_type as $rs) { ?>
                           <option value="<?php echo $rs['eq_id']; ?>" <?php if($type == $rs['eq_id']){ echo "selected"; } ?> ><?php echo $rs['eq_name']; ?></option>
                         <?php } ?>
                      </select>
                  &nbsp;&nbsp;
                  &nbsp;&nbsp;
                  <label for="eq_code">RQ Status:</label>
                    <select class="form-control" id="rq_status" name="rq_status" style="width: 120px;">
                        <option value="All" <?php if($rq_status == 'All'){ echo "selected"; } ?> >All</option>
                        <option value="None" <?php if($rq_status == 'None'){ echo "selected"; } ?> >None</option>
                        <option value="Process" <?php if($rq_status == 'Process'){ echo "selected"; } ?> >Process</option>
                        <option value="Wait" <?php if($rq_status == 'Wait'){ echo "selected"; } ?> >Wait</option>
                        <option value="Complete" <?php if($rq_status == 'Complete'){ echo "selected"; } ?> >Complete</option>
                    </select>
                    &nbsp;&nbsp;
                  &nbsp;&nbsp;
                  <label for="type">Request Type</label>
                      <select class="form-control" id="eq_no" name="eq_no" style="width: 120px;">
                        <option value="All" <?php if($eq_no == 'All'){ echo "selected"; } ?>>All</option>
                        <option value="BD" <?php if($eq_no == 'BD'){ echo "selected"; } ?>>Breakdown</option>
                        <option value="PM" <?php if($eq_no == 'PM'){ echo "selected"; } ?>>Maintenance</option>
                        <option value="OT" <?php if($eq_no == 'PM'){ echo "selected"; } ?>>Other</option>
                      </select>
                  &nbsp;&nbsp;
                  <label for="type">Request Code</label>
                      <input class="form-control" type="text" class="form-control" name="rq_code" value="<?php echo $rq_code ?>">
                  &nbsp;&nbsp; <br> <br> 
                  <label>&nbsp;&nbsp;From Date :</label>
                  <input class="form-control" type="date" class="form-control" name="first_date" value="<?php echo $first_date ?>">
                  &nbsp;&nbsp;
                  <label>To Date :</label>
                   <input class="form-control" type="date" class="form-control" name="second_date" value="<?php echo $second_date ?>">
                  <button class="btn btn-info"><i class="icon-search"></i>&nbsp;Search</button>
                  &nbsp;&nbsp;
                </form>
            </div>
            <!-- /widget-content --> 
          </div>

        <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header" id="print-btn">&nbsp;
              <a href="<?php echo site_url(); ?>Reports/PrintReport/<?php echo $rq_status; ?>/<?php echo $type; ?>/<?php echo $eq_no; ?>/<?php echo $first_date; ?>/<?php echo $second_date; ?>/<?php echo $rq_code ?>" target="_blank"><button class="btn btn-info">Print</button></a>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered" style="font-size: 12px;" id="table-report">
                <thead >
                  <tr>
                    <th>
                     RQ
                    </th>
                    <th style="text-align: left;">EQ</th>
                    <th style="text-align: left;">Number</th>
                    <th style="text-align: left;">RQ Type</th>
                    <th style="text-align: left;"> Code</th>
                    <th style="text-align: left;"> Detail</th>
                    <th style="text-align: center;"> Status</th>
                    <th style="text-align: left;">Lost Time</th>
                    <th style="text-align: left;">RQ Date</th>
                    <th style="text-align: left;">AC Date</th>
                    <th style="text-align: left;">CM Date</th>
                    <th style="text-align: left;">Remark</th>
                  </tr>
                </thead> 
                <tbody>

                <?php if($rq_order) { ?>
                      <?php $i=1; foreach ($rq_order as $rs) { ?>

                      <tr class="r-eq"  data-rq_id="<?php echo $rs['rq_id']; ?>">
                        <td>
                          <?php echo $rs['rq_id']; ?>
                        </td>
                        <td>
                          <?php echo $rs['eq_name']; ?>
                        </td>
                        <td>
                          <?php echo $rs['eq_code'].$rs['eq_no']; ?>
                        </td>
                        <td>
                          <?php if($rs['rq_type'] == 'BD' ){ ?>
                              <span class="label label-danger">Breakdown</span>
                            <?php } else if($rs['rq_type'] == 'PM' ) { ?>
                              <span class="label label-warning">Maintenance</span>
                            <?php } else if($rs['rq_type'] == 'OT' ) { ?>
                               <span class="label label-info">Other</span>
                            <?php } ?>

                        </td>
                        <td>
                          <?php echo $rs['rq_code']; ?>
                        </td>
                        <td>
                          <?php echo $rs['rq_detail']; ?>
                        </td>
                        <td style="text-align: center;">
                          <?php if($rs['rq_accept'] == 'None' ){ ?>
                              <a href="#add" class="update-status"><span class="label label-default"><?php echo $rs['rq_accept']; ?></span></a>
                            <?php } else if($rs['rq_accept'] == 'ReadyRQ' ) { ?>
                               <a href="#add" class="update-status"><span class="label label-default"><?php echo $rs['rq_accept']; ?></span></a> 
                            <?php } else if($rs['rq_accept'] == 'WaitRQ' ) { ?>
                               <a href="#add" class="update-status"><span class="label label-default"><?php echo $rs['rq_accept']; ?></span></a> 
                            <?php } else if($rs['rq_accept'] == 'Process' ) { ?>
                              <a href="#add" class="update-status"><span class="label label-info"><?php echo $rs['rq_accept']; ?></span></a>
                            <?php } else if($rs['rq_accept'] == 'Complete' ) { ?>
                              <span class="label label-success" style="cursor: not-allowed;"><?php echo $rs['rq_accept']; ?></span>
                            <?php } else if($rs['rq_accept'] == 'Wait' ) { ?>
                              <a href="#add" class="update-status" title="<?php echo $rs['remark_w']; ?>"><span class="label label-warning"><?php echo $rs['rq_accept']; ?></span></a>
                            <?php } ?>
                        </td>
                        <td>
                          <?php 

                            if($rs['accept_date'] != ''){

                              if($rs['complete_date'] != ''){

                                $date = new DateTime($rs['accept_date']);
                                $now = new DateTime($rs['complete_date']);
                                echo $date->diff($now)->format("%d D: %h H: %i M");

                              } else {

                                $date = new DateTime($rs['accept_date']);
                                $now = new DateTime();
                                echo $date->diff($now)->format("%d D: %h H: %i M");

                              }

                            } else {
                              echo '-';
                            }

                          ?>
                        </td>
                        <td>
                          <?php echo date("j-M-y H:i", strtotime($rs['created'])); ?>
                        </td>
                        <td>
                          <?php 
                            if($rs['accept_date'] != ''){
                              echo date("j-M-y H:i", strtotime($rs['accept_date'])); 
                            } else {
                              echo '-';
                            }
                          ?>
                        </td>
                        <td>
                          <?php 
                            if($rs['complete_date'] != ''){
                              echo date("j-M-y H:i", strtotime($rs['complete_date'])); 
                            } else {
                              echo '-';
                            }
                          ?>
                        </td>
                        <td>
                            <?php 
                            if($rs['remark_w'] != ''){
                              echo $rs['remark_w']; 
                            } else {
                              echo '-';
                            }
                          ?>
                        </td>
                      </tr>

                      <?php $i++;  } ?>
                  <?php } else { ?>
                    <tr >
                      <td colspan="12" style="text-align: center;">-No Record-</td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 


          </div>
</div>
<script src="<?php echo base_url(); ?>public/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript">

    function myFunction() {

      $('#search-bar').css("display", "none");
      $('#print-btn').css("display", "none"); 
      $('#menu-bar').css("display", "none");
      $('#header-bar').css("display", "none");
      $('#table-report').css("font-size", "8px");
      $('#table-report-t').css("font-size", "8px");

      //window.print();

      //location.reload();
    }


$(document).ready(function(){

});
</script>