<style type="text/css">


  @media only screen and (max-width: 600px){
    #no_th{
      display: none;
    }
    #no_td{
      display: none;
    }

    #cre_th{
      display: none;
    }

    #cre_td{
      display: none;
    }

    #up_th{
      display: none;
    }

    #up_td{
      display: none;
    }

    .edit-user{
      display: none;
    }

    .del-user{
      display: none;
    }

  }

}


</style>

  <!-- Add EQ -->
  <div class="modal fade" id="add-rq-bd" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Users</h4>
        </div>
        <div class="modal-body">
              <label for="eq_code">Name:</label>
              <input type="text" name="name" id="name" class="form-control" style="width: 100%;">
              <label for="eq_code">Username:</label>
              <input type="text" name="username" id="username" class="form-control" style="width: 100%;">
              <!--<label for="eq_code">Password:</label>
              <input type="text" name="password" id="password" class="form-control" style="width: 500px;">
              <label for="eq_code">Confirm-Password:</label>
              <input type="text" name="con_password" id="con_password" class="form-control" style="width: 500px;">-->
              <label for="eq_code">Permission:</label>
              <select class="form-control" id="permission" style="width: 100%;">
                  <option value="0">--</option>
                  <option value="ADMIN">ADMIN</option>
                  <option value="TECHNICIAN">TECHNICIAN</option>
                  <option value="MANAGEMENT">MANAGEMENT</option>
                  <option value="SUPPORT">SUPPORT</option>
              </select>
              <p id="msg-error-eq" style="color: red;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success save-rq" >Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>


    <!-- Add EQ -->
  <div class="modal fade" id="edit-rq-bd" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Users</h4>
        </div>
        <div class="modal-body">
              <label for="eq_code">Name:</label>
              <input type="text" name="name" id="edit-name" class="form-control" style="width: 100%;">
              <label for="eq_code">Username:</label>
              <input type="text" name="username" id="edit-username" class="form-control" style="width: 100%;">
              <input type="hidden" id="edit-id">
              <!--<label for="eq_code">Password:</label>
              <input type="text" name="password" id="password" class="form-control" style="width: 500px;">
              <label for="eq_code">Confirm-Password:</label>
              <input type="text" name="con_password" id="con_password" class="form-control" style="width: 500px;">-->
              <label for="eq_code">Permission:</label>
              <select class="form-control" id="edit-permission" style="width: 100%;">
                  <option value="0">--</option>
                  <option value="ADMIN">ADMIN</option>
                  <option value="TECHNICIAN">TECHNICIAN</option>
                  <option value="MANAGEMENT">MANAGEMENT</option>
              </select>
              <p id="msg-error-eq" style="color: red;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success edit-rq" >Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>


<div class="span12">

  <div class="widget">
            <div class="widget-header"> <i class="icon-user"></i>
              <h3>Users</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
             
                  <table class="table table-striped table-bordered" style="font-size: 12px;">
                    <thead>
                      <tr>
                        <td style="text-align: center; width: 10%;" id="no_th"><b>#</b></td>
                        <td style="text-align: left;">
                           <div class="dropdown pull-left"> <a class="dropdown-toggle " id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="#">Name <i class=" icon-caret-down"></i> </a>
                          <ul class="dropdown-menu " role="menu" aria-labelledby="dLabel">
                            <li><a href="#add-rq-bd" data-toggle="modal" data-target="#add-rq-bd"><i class=" icon-plus icon-small"></i>&nbsp; &nbsp; Add</a></li>
                          </ul>
                        </div>
                        </td>
                        <td style="text-align: left;"><b>Username</b> </td>
                        <td style="text-align: left;"><b>Permission</b></td>
                        <td style="text-align: left;" id="cre_th"><b>Created</b></td>
                        <td style="text-align: left;" id="up_th"><b>Updated</b></td>
                        <td style="text-align: center;"></td>
                      </tr>
                    </thead>
                    <tbody>
                       <?php $i=1; foreach ($user as $rs) { ?>
                      <tr class="tr-user" data-user="<?php echo $rs['id']; ?>" data-name="<?php echo $rs['name']; ?>" data-username="<?php echo $rs['username']; ?>" data-role ="<?php echo $rs['role']; ?>">
                        <td style="text-align: center;" id="no_td"><?php echo $i; ?></td>
                        <td style="text-align: left;"><?php echo $rs['name']; ?></td>
                        <td style="text-align: left;"><?php echo $rs['username']; ?></td>
                        <td style="text-align: left;"><?php echo $rs['role']; ?></td>
                        <td style="text-align: left;" id="cre_td"><?php echo date("j-M-y H:i", strtotime($rs['created'])); ?></td>
                        <td style="text-align: left;" id="up_td"><?php echo date("j-M-y H:i", strtotime($rs['updated'])); ?></td>
                        <td style="text-align: center;">
                          <button class="btn btn-info re-user"><i class="icon-refresh"></i></button>
                          <button class="btn btn-warning edit-user"><i class="icon-pencil"></i></button>
                          <button class="btn btn-danger del-user"><i class="icon-trash"></i></button>
                        </td>
                      </tr>
                      <?php $i++;  } ?>
                    </tbody>
                  </table>
                  
                <!-- /shortcuts --> 

            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->

</div>


<script src="<?php echo base_url(); ?>public/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript">
$(document).ready(function(){

   $('.save-rq').click(function(){
       
      var name = $('#name').val();
      var username = $('#username').val();
      /*var password = $('#password').val();
      var con_password = $('#con_password').val();*/
      var permission = $('#permission').val();

      if(name == '' || name == null){
        $('#msg-error-eq').html('*Please Input Name');
      } else if(username == '' || username == null){
        $('#msg-error-eq').html('*Please Input Username');
      }  else if(permission == 0 || permission == null){
        $('#msg-error-eq').html('*Password Select permission');
      } else {

        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Admin/SaveUsers',
          data:{ name:name, username:username,  permission:permission}
        }).done(function(data){
            var o = JSON.parse(data);
            alert(o.msg);

            if(o.code_m == 'complete'){
              location.reload();
            }
        });

      }

  }); 

  $("table").off("click", ".edit-user");
  $("table").on("click", ".edit-user", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.tr-user');
      var user = $row.data('user');
      var name = $row.data('name');
      var username = $row.data('username');
      var role = $row.data('role');

      $('#edit-id').val(user);
      $('#edit-name').val(name);
      $('#edit-username').val(username);
      $('#edit-permission').val(role);

       $('#edit-rq-bd').modal('show');


  });

    $('.edit-rq').click(function(){
      
      var id = $('#edit-id').val();
      var name = $('#edit-name').val();
      var username = $('#edit-username').val();
      var permission = $('#edit-permission').val();


        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Admin/SaveUsers',
          data:{ id:id, name:name, username:username,  permission:permission}
        }).done(function(data){
            var o = JSON.parse(data);
            alert(o.msg);

            if(o.code_m == 'complete'){
              location.reload();
            }
        });


  }); 


  $("table").off("click", ".re-user");
  $("table").on("click", ".re-user", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.tr-user');
      var id = $row.data('user');
      

      var txt;
      var r = confirm("Are you sure to reset password ?");
      if (r == true) {
        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Admin/Reset',
          data:{ id:id}
        }).done(function(data){
            var o = JSON.parse(data);
            alert(o.msg);

            if(o.code_m == 'complete'){
              location.reload();
            }
        });

      } 

  });

  $("table").off("click", ".del-user");
  $("table").on("click", ".del-user", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.tr-user');
      var id = $row.data('user');
      

      var txt;
      var r = confirm("Are you sure to delete user ?");
      if (r == true) {
        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Admin/DeleteUsers',
          data:{ id:id}
        }).done(function(data){
            var o = JSON.parse(data);
            alert(o.msg);

            if(o.code_m == 'complete'){
              location.reload();
            }
        });

      } 

  });

});
</script>