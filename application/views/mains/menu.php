<div class="subnavbar" id="menu-bar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <li <?php if($this->uri->segment(1)=="Dashboard" or $this->uri->segment(1) == null or $this->uri->segment(1) == "History" ){echo 'class = "active"';}?>><a href="<?php echo site_url(); ?>"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        
        <li <?php if($this->uri->segment(1)=="Reports" ){echo 'class = "active"';}?>><a href="<?php echo site_url(); ?>Reports"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>
        
        <!--<li <?php if($this->uri->segment(1)=="Breakdown"){echo 'class = "active"';} ?>><a href="<?php echo site_url(); ?>Breakdown"><i class="icon-plus-sign"></i><span>RQ Breakdown</span> </a></li>
        <li <?php if($this->uri->segment(1)=="Maintenance"){echo 'class = "active"';} ?>><a href="<?php echo site_url(); ?>Maintenance"><i class="icon-plus-sign"></i><span>RQ Maintenance</span> </a></li>
        <li <?php if($this->uri->segment(1)=="Other"){echo 'class = "active"';} ?>><a href="<?php echo site_url(); ?>Other"><i class="icon-plus-sign"></i><span>RQ Other</span> </a></li>-->
        <li class="dropdown <?php if($this->uri->segment(1)=="Breakdown" OR $this->uri->segment(1)=="Maintenance" OR $this->uri->segment(1)=="Other"){echo 'active';}?>">
          <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-plus-sign"></i><span>Request</span> 
          </a>

          <ul class="dropdown-menu">
            <li <?php if($this->uri->segment(1)=="Breakdown"){echo 'class = "active"';} ?>>
              <a href="<?php echo site_url(); ?>Breakdown">Breakdown</a>
            </li>

            <li <?php if($this->uri->segment(1)=="Maintenance"){echo 'class = "active"';} ?>>
              <a href="<?php echo site_url(); ?>Maintenance">Maintenance</a>
            </li>

            <li <?php if($this->uri->segment(1)=="Other"){echo 'class = "active"';} ?>>
              <a href="<?php echo site_url(); ?>Other">Other</a>
            </li>
          </ul>

        </li>
        <?php if($user->role == "ADMIN" or $user->role == "MANAGEMENT"){ ?>
        <li class="dropdown <?php if($this->uri->segment(1)=="Setting"){echo 'active';}?>"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-cogs"></i><span>Setting</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">

            <?php if($user->role == "ADMIN"){ ?>
              <li <?php if($this->uri->segment(1)=="Admin"){echo 'class = "active"';} ?>><a href="<?php echo site_url(); ?>Admin"><span>Admin</span> </a></li>
            <?php } ?>

            <li <?php if($this->uri->segment(2)=="Equipment"){echo 'class = "active"';} else if($this->uri->segment(2)=="EquipmentDetail"){echo 'class = "active"';} ?> ><a href="<?php echo site_url(); ?>Setting/Equipment">Equipment</a></li>
            <li <?php if($this->uri->segment(2)=="Breakdown"){echo 'class = "active"';} else if($this->uri->segment(2)=="BreakdownDetail"){echo 'class = "active"';} else if($this->uri->segment(2)=="BreakdownSystemDetail"){echo 'class = "active"';}  ?> ><a href="<?php echo site_url(); ?>Setting/Breakdown">Breakdown</a></li>
            <li <?php if($this->uri->segment(2)=="Maintenance"){echo 'class = "active"';} else if($this->uri->segment(2)=="MaintenanceService"){echo 'class = "active"';} ?> ><a href="<?php echo site_url(); ?>Setting/Maintenance">Maintenance</a></li>
            <li <?php if($this->uri->segment(2)=="Other"){echo 'class = "active"';} else if($this->uri->segment(2)=="OtherService"){echo 'class = "active"';} ?> ><a href="<?php echo site_url(); ?>Setting/Other">Other</a></li>

          </ul>
        </li>
        <?php } ?>

        <li></li>
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>