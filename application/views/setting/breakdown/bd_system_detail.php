<style type="text/css">
  .label-danger {
    background-color: #db3325;
}
</style>

<div class="span12">
         <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3><a href="<?php echo site_url(); ?>setting/Breakdown"> Breakdown </a></h3>
              <div align="right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered" style="font-size: 12px;">
                <thead>
                  <tr>
                    <th style="width: 60%;font-size: 12px;">
                     Type
                    </th>
                    <th style="width: 10%;font-size: 12px; text-align: center;">Code</th>
                  </tr>
                </thead>
                <tbody>

                  <tr class="r-eq" data-eq_id="<?php echo $bd->eq_id; ?>">
                    <td>
                      <?php echo $bd->eq_name; ?>
                    </td>
                    <td style="width: 10%;font-size: 12px; text-align: center;"> <?php echo $bd->eq_code; ?></td>
                  </tr>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
</div>

<div class="span10">
        <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3><a href="<?php echo site_url(); ?>setting/BreakdownDetail/<?php echo $bd_system->eq_id; ?>">Breakdown System</a></h3>
              <div align="right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered" style="font-size: 12px;">
                <thead>
                  <tr>
                    <th style="font-size: 12px;">
                     System Name
                    </th>
                    <th style="font-size: 12px; text-align: center;">Code</th>
                  </tr>
                </thead>
                <tbody>


                    <?php if($bd_system){ ?>

                    <tr class="r-bd" data-bd_id="<?php echo $bd_system->bd_id; ?>" data-bd_name="<?php echo $bd_system->bd_name; ?>" data-bd_code="<?php echo $bd_system->bd_code; ?>">
                      <td style="font-size: 12px; "><?php echo $bd_system->bd_name; ?></td>
                      <td style="font-size: 12px; text-align: center;"><?php echo $bd_system->bd_code; ?></td>
                    </tr>

                    <?php } else { ?>

                       <tr class="r-bd" ">
                        <td style="font-size: 12px; text-align: center;" colspan="2">-No Data-</td>
                      </tr>

                    <?php } ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
</div>



<div class="span8">
        <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Breakdown Detail</a></h3>
              <div align="right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered" style="font-size: 12px;">
                <thead>
                  <tr>
                    <th style="font-size: 12px; text-align: center;">#</th>
                    <th style="font-size: 12px;">
                      <div class="dropdown pull-left"> <a class="dropdown-toggle " id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="#">System Detail <i class=" icon-caret-down"></i> </a>
                          <ul class="dropdown-menu " role="menu" aria-labelledby="dLabel">
                            <li style="cursor: pointer;"><a class="ad-system-detail"><i class=" icon-plus icon-small"></i>&nbsp; &nbsp; Add</a></li>
                          </ul>
                        </div>
                    </th>
                    </th>
                    <th style="font-size: 12px; text-align: center;">Code</th>
                    <th style="text-align: center;"></th>
                  </tr>
                </thead>
                <tbody>


                    <?php if($bd_detail){ ?>
                      <?php $i=1;  foreach ($bd_detail as $rs) {  ?>

                      <tr class="r-bd" data-bdd_id="<?php echo $rs['bdd_id']; ?>" data-bdd_code="<?php echo $rs['bdd_code']; ?>" data-bd_detail="<?php echo $rs['bd_detail']; ?>">
                        <td style="font-size: 12px; text-align: center;"><?php echo $i; ?></td>
                        <td style="font-size: 12px; "><?php echo $rs['bd_detail']; ?></td>
                        <td style="font-size: 12px; text-align: center;"><?php echo $rs['bdd_code']; ?></td>
                        <td style="text-align: center;">
                          <a class="btn btn-small btn-warning edit-bd-system" title="Edit"><i class=" icon-pencil icon-small"> </i></a>
                          <a class="btn-small btn btn-danger del-bd-system" title="Delete"><i class="icon-trash icon-small"> </i></a>
                        </td>
                      </tr>

                      <?php $i++;  } ?>

                    <?php } else { ?>

                      <tr class="r-bd" ">
                        <td style="font-size: 12px; text-align: center;" colspan="3">-No Data-</td>
                      </tr>

                    <?php } ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
</div>

  <!-- Add EQ -->
  <div class="modal fade" id="ad-system-detail-form" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add System Detail</h4>
        </div>
        <div class="modal-body">
              <label for="bdd_code">System Code:</label>
              <input type="text" class="form-control" id="bdd_code" name="bdd_code" style="width: 500px;">
              <input type="hidden" id="bd_id" value="<?php echo $bd_system->bd_id; ?>">
              <label for="bd_detail">System Detail:</label>
              <textarea class="form-control" rows="5" id="bd_detail" name="bd_detail" style="width: 500px;"></textarea>
              <p id="msg-error-eq" style="color: red;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success save-system-detail" >Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>


  <!-- Add EQ -->
  <div class="modal fade" id="edit-system-detail-form" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit System Detail</h4>
        </div>
        <div class="modal-body">
              <label for="bdd_code">System Code:</label>
              <input type="text" class="form-control" id="edit_bdd_code" name="edit_bdd_code" style="width: 500px;">
              <input type="hidden" id="edit_bdd_id">
              <label for="bd_detail">System Detail:</label>
              <textarea class="form-control" rows="5" id="edit_bd_detail" name="edit_bd_detail" style="width: 500px;"></textarea>
              <p id="msg-edit-error-eq" style="color: red;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success edit-system-detail" >Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>
 
 <!-- Delete EQ -->
  <div class="modal fade" id="del-bd-detail-form" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete  System Detail</h4>
        </div>
        <div class="modal-body">
              <p>Confirm to delete ?</p>
              <input type="hidden" id="del_bdd_id" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success cf-del-bd-detail" >Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>

<script src="<?php echo base_url(); ?>public/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript">
$(document).ready(function(){

  $("table").off("click", ".ad-system-detail");
  $("table").on("click", ".ad-system-detail", function(e) {
      e.preventDefault();
      
      $('#ad-system-detail-form').modal('show');

  });

   $('.save-system-detail').click(function(){
      
      var bd_id = $('#bd_id').val();
      var bdd_code = $('#bdd_code').val();
      var bd_detail = $('#bd_detail').val();

      if(bdd_code == ''){
        $('#msg-error-eq').html('*Please Input System Detail Code');
      } else if(bd_detail == ''){
        $('#msg-error-eq').html('*Please Input System Detail');
      } else {
        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Setting/SaveBreakdownSystemDetail',
          data:{ bd_id:bd_id, bdd_code:bdd_code, bd_detail:bd_detail }
        }).done(function(data){
            var o = JSON.parse(data);

            if(o.code_m == 'error'){
              $('#msg-error-eq').html(o.msg);
            }
            
            if(o.code_m == 'complete'){
              location.reload();
            }
        })

      }
         
  });

  $("table").off("click", ".edit-bd-system");
  $("table").on("click", ".edit-bd-system", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.r-bd');
      var bdd_id = $row.data('bdd_id');
      var bdd_code = $row.data('bdd_code');
      var bd_detail = $row.data('bd_detail');

      $('#edit_bdd_id').val(bdd_id);
      $('#edit_bdd_code').val(bdd_code);
      $('#edit_bd_detail').val(bd_detail);
      
      $('#edit-system-detail-form').modal('show');

  });

  $('.edit-system-detail').click(function(){
       
      var bdd_id = $('#edit_bdd_id').val();
      var bdd_code = $('#edit_bdd_code').val();
      var bd_detail = $('#edit_bd_detail').val();

      if(bdd_code == ''){
        $('#msg-edit-error-eq').html('*Please Input Equipment Code');
      } else {
        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Setting/EditBreakdownSystemDetail',
          data:{ bdd_id:bdd_id, bdd_code:bdd_code, bd_detail:bd_detail }
        }).done(function(data){
            var o = JSON.parse(data);

            if(o.code_m == 'error'){
              $('#msg-edit-error-eq').html(o.msg);
            }
            
            if(o.code_m == 'complete'){
              location.reload();
            }
        })

      }
         
  });

  $("table").off("click", ".del-bd-system");
  $("table").on("click", ".del-bd-system", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.r-bd');
      var bdd_id = $row.data('bdd_id');

      $('#del_bdd_id').val(bdd_id);
      
      $('#del-bd-detail-form').modal('show');

  });


  $('.cf-del-bd-detail').click(function(){
       
      var bdd_id = $('#del_bdd_id').val();

        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Setting/DelBreakdownSystemDetail',
          data:{ bdd_id:bdd_id}
        }).done(function(data){
            var o = JSON.parse(data);

            if(o.code_m == 'complete'){
              location.reload();
            }
        })

         
  });
 
});         
</script>
