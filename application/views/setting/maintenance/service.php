<style type="text/css">
  .label-danger {
    background-color: #db3325;
}
</style>

<div class="span12">
         <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3><a href="<?php echo site_url(); ?>setting/Maintenance"> Maintenance Service </a></h3>
              <div align="right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered" style="font-size: 12px;">
                <thead>
                  <tr>
                    <th style="width: 60%;font-size: 12px;">
                      Service Type
                    </th>
                    </th>
                    <th style="width: 10%;font-size: 12px; text-align: center;">Code</th>
                  </tr>
                </thead>
                <tbody>

                  <tr class="r-eq" >
                    <td>
                      <?php echo $bd->eq_name; ?>
                      <input type="hidden" id="eq_id" value="<?php echo $bd->eq_id; ?>">
                    </td>
                    <td style="width: 10%;font-size: 12px; text-align: center;"><?php echo $bd->eq_code; ?></td>
                  </tr>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
</div>

<div class="span10">
        <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Service Detail</h3>
              <div align="right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered" style="font-size: 12px;">
                <thead>
                  <tr>
                    <th style="font-size: 12px; text-align: center;">
                      <div class="dropdown pull-left"> <a class="dropdown-toggle " id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="#">Code <i class=" icon-caret-down"></i> </a>
                          <ul class="dropdown-menu " role="menu" aria-labelledby="dLabel">
                            <li style="cursor: pointer;"><a class="ad-system"><i class=" icon-plus icon-small"></i>&nbsp; &nbsp; Add</a></li>
                          </ul>
                        </div>
                    </th>
                    <th style="font-size: 12px;">
                     Service Name 
                    </th>
                    <th style="font-size: 12px; text-align: left;">Detail</th>
                    <th style="width: 15%;"> </th>
                  </tr>
                </thead>
                <tbody>

                <?php if($service){ ?>
                  <?php foreach ($service as $rs) { ?>

                    <tr class="r-bd" data-ms_id="<?php echo $rs['ms_id']; ?>" data-ms_name="<?php echo $rs['ms_name']; ?>" data-ms_detail="<?php echo $rs['ms_detail']; ?>" data-ms_code="<?php echo $rs['ms_code']; ?>">
                      <td style="font-size: 12px; text-align: center;"><?php echo $rs['ms_code']; ?></td>
                      <td style="font-size: 12px; "><?php echo $rs['ms_name']; ?></td>
                      <td style="font-size: 12px; text-align: left;"><?php echo $rs['ms_detail']; ?></td>
                      <td class="td-actions">
                        <a class="btn btn-small btn-warning edit-bd-system" title="Edit"><i class=" icon-pencil icon-small"> </i></a>
                        <a class="btn-small btn btn-danger del-bd-system" title="Delete"><i class="icon-trash icon-small"> </i></a>
                      </td>
                    </tr>

                    <?php } ?>

                  <?php } else { ?>

                    <tr>
                      <td colspan="5" style="text-align: center;">-No Data-</td>
                    </tr>

                  <?php } ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
</div>

  <!-- Add EQ -->
  <div class="modal fade" id="add-eq-type" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Maintenance Service</h4>
        </div>
        <div class="modal-body">
              <label for="eq_code">Service Name:</label>
              <input type="text" class="form-control" id="ms_name" name="bd_name" style="width: 500px;">
              <input type="hidden" id="ms_id">

              <label for="eq_code">Service Code:</label>
              <input type="text" class="form-control" id="ms_code" name="bd_code" style="width: 500px;">

              <label for="bd_detail">Service Detail:</label>
              <textarea class="form-control" rows="5" id="ms_detail" name="ms_detail" style="width: 500px;"></textarea>
              <p id="msg-error-eq" style="color: red;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success save-system" >Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>

    <!-- Add EQ -->
  <div class="modal fade" id="edit-bd-form" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Breakdown System</h4>
        </div>
        <div class="modal-body">
              <label for="eq_code">Service Name:</label>
              <input type="text" class="form-control" id="edit_ms_name" name="ms_name" style="width: 500px;">
              <input type="hidden" id="edit_ms_id">

              <label for="eq_code">Service Code:</label>
              <input type="text" class="form-control" id="edit_ms_code" name="ms_code" style="width: 500px;">

              <label for="bd_detail">Service Detail:</label>
              <textarea class="form-control" rows="5" id="edit_ms_detail" name="ms_detail" style="width: 500px;"></textarea>
              <p id="msg-error-edit-bd" style="color: red;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success edit-system" >Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>

      <!-- Delete EQ -->
  <div class="modal fade" id="del-bd-form" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Service Detail</h4>
        </div>
        <div class="modal-body">
              <p>Confirm to delete ?</p>
              <input type="hidden" id="del_ms_id" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success cf-del-bd" >Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>


<script src="<?php echo base_url(); ?>public/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript">
$(document).ready(function(){

  $("table").off("click", ".ad-system");
  $("table").on("click", ".ad-system", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.r-eq');
      var eq_id = $('#eq_id').val();

      $('#add_eq_type').val(eq_id);

       $('#add-eq-type').modal('show');


  });

  $('.save-system').click(function(){
       
      var eq_id = $('#eq_id').val();
      var ms_name = $('#ms_name').val();
      var ms_code = $('#ms_code').val();
      var ms_detail = $('#ms_detail').val();

      if(ms_name == ''){
        $('#msg-error-edit-eq').html('*Please Input Service Name');
      } else if(ms_code == ''){
        $('#msg-error-edit-eq').html('*Please Input Service Code');
      } else if(ms_detail == ''){
        $('#msg-error-edit-eq').html('*Please Input Service Detail');
      } else {
        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Setting/SaveMaintenanceService',
          data:{ eq_id:eq_id, ms_name:ms_name, ms_code:ms_code, ms_detail:ms_detail}
        }).done(function(data){
            var o = JSON.parse(data);

            if(o.code_m == 'error'){
              $('#msg-error-edit-eq').html(o.msg);
            }
            

            if(o.code_m == 'complete'){
              location.reload();
            }
        })

      }

  });

  $("table").off("click", ".edit-bd-system");
  $("table").on("click", ".edit-bd-system", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.r-bd');
      var ms_id = $row.data('ms_id');
      var ms_name = $row.data('ms_name');
      var ms_code = $row.data('ms_code');
      var ms_detail = $row.data('ms_detail');

      $('#edit_ms_name').val(ms_name);
      $('#edit_ms_code').val(ms_code);
      $('#edit_ms_detail').val(ms_detail);
      $('#edit_ms_id').val(ms_id);

       $('#edit-bd-form').modal('show');


  });

  $('.edit-system').click(function(){
       
      var eq_id = $('#eq_id').val();
      var ms_name = $('#edit_ms_name').val();
      var ms_code = $('#edit_ms_code').val();
      var ms_detail = $('#edit_ms_detail').val();
      var ms_id = $('#edit_ms_id').val();

      if(ms_name == ''){
        $('#msg-error-eq').html('*Please Input Service Name');
      } else if(ms_code == ''){
        $('#msg-error-eq').html('*Please Input Service Code');
      } else if(ms_detail == ''){
        $('#msg-error-eq').html('*Please Input Service Detail');
      } else {
        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Setting/EditMaintenanceService',
          data:{ eq_id:eq_id, ms_name:ms_name, ms_code:ms_code, ms_detail:ms_detail, ms_id:ms_id}
        }).done(function(data){
            var o = JSON.parse(data);

            if(o.code_m == 'error'){
              $('#msg-error-edit-bd').html(o.msg);
            }
            

            if(o.code_m == 'complete'){
              location.reload();
            }
        })

      }

  });

  $("table").off("click", ".del-bd-system");
  $("table").on("click", ".del-bd-system", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.r-bd');
      var ms_id = $row.data('ms_id');

      $('#del_ms_id').val(ms_id);

      $('#del-bd-form').modal('show');


  });


  $('.cf-del-bd').click(function(){
       
      var ms_id = $('#del_ms_id').val();

        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Setting/DelMaintenanceService',
          data:{ms_id:ms_id}
        }).done(function(data){
            var o = JSON.parse(data);

            if(o.code_m == 'complete'){
              location.reload();
            }
        })


  });

 
});         
</script>
