<div class="span10">
        <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Other Service</h3>
              <div align="right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered" style="font-size: 12px;">
                <thead>
                  <tr>
                    <th style="width: 60%;font-size: 12px;">
                     Type
                    </th>
                    <th style="width: 10%;font-size: 12px; text-align: center;">Code</th>
                    <th style="width: 20%;"> </th>
                  </tr>
                </thead>
                <tbody>

                  <?php foreach ($bd as $rs) { ?>

                  <tr class="r-eq">
                    <td>
                      <?php echo $rs['eq_name']; ?>
                    </td>
                    <td style="width: 10%;font-size: 12px; text-align: center;"><?php echo $rs['eq_code']; ?></td>
                    <td class="td-actions">
                      <a href="<?php echo site_url(); ?>Setting/OtherService/<?php echo $rs['eq_id']; ?>" class="btn btn-small btn-success" title="Add"><i class="icon-plus icon-small"> </i></a>
                    </td>
                  </tr>

                  <?php } ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
</div>


<script src="<?php echo base_url(); ?>public/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript">
$(document).ready(function(){

  
});
</script>