<style type="text/css">
  .label-danger {
    background-color: #db3325;
}

   div.scroll { 
               overflow:scroll; height:400px;
            } 

</style>


<div class="span6">

  <div class="widget">
            <div class="widget-header"> <i class="icon-bullhorn"></i>
              <h3>Summary</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php foreach ($summary as $rs) { ?>
                <h3><u><?php echo $rs->eq_name; ?> <?php echo $rs->eq_code; ?></u></h3>
                <?php if ($rs->total != 0) { ?>
                  <table class="table table-striped table-bordered" style="font-size: 12px;">
                    <thead>
                      <tr>
                        <td style="text-align: center; width: 20%;"><b>All</b></td>
                        <td style="text-align: center; width: 20%;"><b>AVA</b> <i class="icon-ok-sign" style="color: green;"></i></td>
                        <td style="text-align: center; width: 20%;"><b>PM</b> <i class="icon-info-sign" style="color: orange;"></i></td>
                        <td style="text-align: center; width: 20%;"><b>BD</b> <i class="icon-remove-sign" style="color: red;"></i></td>
                        <td style="text-align: center; width: 20%;"><b>OT</b> <i class="icon-question-sign" style="color: blue;"></i></td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td style="text-align: center;"><b><?php echo $rs->total; ?></b></td>
                        <td style="text-align: center;"><b><?php echo $rs->ava+$rs->ot; ?></b></td>
                        <td style="text-align: center;"><b><?php echo $rs->man; ?></b></td>
                        <td style="text-align: center;"><b><?php echo $rs->bd; ?></b></td>
                        <td style="text-align: center;"><b><?php echo $rs->ot; ?></b></td>
                      </tr>
                    </tbody>
                  </table>
                  <?php } ?>
                <!-- /shortcuts --> 
              <?php } ?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->

</div>


<div class="span6">
        <!-- /widget --> 
          <div class="widget widget-nopad scroll">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3>New Request</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <ul class="news-items">
                <?php if($rq_order) { foreach ($rq_order as $rs) { ?>
                  <li>
                    
                    <div class="news-item-date"> <span class="news-item-day"><?php echo date("j", strtotime($rs['updated'])); ?></span> <span class="news-item-month"><?php echo date("M", strtotime($rs['updated'])); ?></span> </div>
                    <div class="news-item-detail"> <a href="<?php if($rs['rq_type'] == 'BD'){ ?> <?php echo site_url(); ?>Breakdown/Detail/<?php echo $rs['eq_id']; ?>/<?php echo $rs['rq_id']; ?> <?php } else if ($rs['rq_type'] == 'PM'){  ?> <?php echo site_url(); ?>Maintenance/Detail/<?php echo $rs['eq_id']; ?>/<?php echo $rs['rq_id']; ?> <?php } else if ($rs['rq_type'] == 'OT'){  ?> <?php echo site_url(); ?>Other/Detail/<?php echo $rs['eq_id']; ?>/<?php echo $rs['rq_id']; ?> <?php } ?>" class="news-item-title" target="_blank">EQ # <?php echo $rs['eq_code'].$rs['eq_no']; ?> <?php if($rs['rq_type'] == 'BD' ){ ?>
                              <span class="label label-danger">Breakdown</span>
                            <?php } else if($rs['rq_type'] == 'PM' ) { ?>
                              <span class="label label-warning">Maintenence</span>
                            <?php } else if($rs['rq_type'] == 'OT' ) { ?>
                              <span class="label label-info">Other</span>
                            <?php } ?> </a>
                      <p class="news-item-preview">
                           <?php echo $rs['rq_code']; ?>  # 
                          <?php echo $rs['rq_detail']; ?> 
                          <?php if($rs['rq_type'] == 'PM'){ ?>
                           <span class="label label-success"><?php echo $rs['rq_accept']; ?></span>
                          <?php } ?>
                       </p>
                    </div>
                    
                  </li>
                <?php } ?>
              <?php } else { ?>
                  <li>
                    
                    <div class="news-item-date"> <span class="news-item-day"></span> </div>
                    <div class="news-item-detail">
                      <p class="news-item-preview" style="text-align: center;">
                         -No Request-
                       </p>
                    </div>
                    
                  </li>

                <?php } ?>
              </ul>
            </div>
            <!-- /widget-content --> 
          </div>

</div>


<div class="span6">
<?php foreach ($eq_type as $rs) { ?>
  <div class="widget">
            <div class="widget-header"> <i class="icon-bullhorn"></i>
              <h3><?php echo $rs['eq_name']; ?> Status
              </h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <form method="POST" action="<?php echo site_url(); ?>Dashboard" enctype="multipart/form-data">
                <div class="form-group" align="right">
                  <select class="form-control" name="eq_type">
                    <?php foreach ($selectType as $rs) { ?>
                      <option value="<?php echo $rs['eq_id']; ?>" <?php if($type == $rs['eq_id']) { echo 'selected'; } ?>>(<?php echo $rs['eq_name']; ?>) <?php echo $rs['eq_code']; ?></option>
                    <?php }?>
                  </select>
                </div>
                <div align="right">
                  <button type="submit" class="btn btn-success">Search</button>
                </div>
              </form>
              <table class="table table-striped table-bordered" style="font-size: 12px;">
                <thead>
                  <tr>
                    <td style="text-align: center; width: 20%;"><b>All</b></td>
                    <td style="text-align: center; width: 20%;"><b>AVA</b> <i class="icon-ok-sign" style="color: green;"></i></td>
                    <td style="text-align: center; width: 20%;"><b>PM</b> <i class="icon-info-sign" style="color: orange;"></i></td>
                    <td style="text-align: center; width: 20%;"><b>BD</b> <i class="icon-ok-sign" style="color: red;"></i></td>
                    <td style="text-align: center; width: 20%;"><b>OT</b> <i class="icon-question-sign" style="color: blue;"></i></td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="text-align: center;"><b><?php echo $ava->c_eq+$man->c_eq+$bd->c_eq+$ot->c_eq; ?></b></td>
                    <td style="text-align: center;"><b><?php echo $ava->c_eq+$ot->c_eq; ?></b></td>
                    <td style="text-align: center;"><b><?php echo $man->c_eq; ?></b></td>
                    <td style="text-align: center;"><b><?php echo $bd->c_eq; ?></b></td>
                    <td style="text-align: center;"><b><?php echo $ot->c_eq; ?></b></td>
                  </tr>
                </tbody>
              </table>
              <div class="shortcuts"> 
                <?php foreach ($eq_detail as $rs) { ?>
                  <a href="<?php echo site_url(); ?>History/Detail/<?php echo $rs['eq_type']; ?>/<?php echo $rs['eq_no']; ?>" class="shortcut" target="blank">
                    <i class="shortcut-icon 
                      <?php if($rs['eq_status'] == 'Available'){
                              echo 'icon-ok-sign';
                            } else if($rs['eq_status'] == 'Maintenance'){ 
                              echo 'icon-info-sign'; 
                            } else if($rs['eq_status'] == 'Breakdown'){ 
                              echo 'icon-remove-sign'; 
                            }  else if($rs['eq_status'] == 'Other'){ 
                              echo 'icon-question-sign'; 
                            } 
                      ?>" style="color: 
                          <?php if($rs['eq_status'] == 'Available'){ 
                                  echo 'green';} 
                                else if($rs['eq_status'] == 'Maintenance'){ 
                                  echo 'orange'; }  
                                else if($rs['eq_status'] == 'Breakdown'){ 
                                  echo 'red'; } 
                                else if($rs['eq_status'] == 'Other'){ 
                                  echo 'blue'; } 
                                ?>">
                   </i><span class="shortcut-label"><b><?php echo $rs['eq_name'].' '.$rs['eq_no']; ?></b></span>
                   <span class="badge badge-pill" style="background-color: #19bc9c;"><?php echo $rs['rq']; ?></span></a>
                <?php } ?>
              </div>
              <!-- /shortcuts --> 

            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->

<?php } ?>
</div>

