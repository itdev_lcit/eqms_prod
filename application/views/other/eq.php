<div class="span10">
        <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Other Request Maintenance</h3>
              <div align="right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered" style="font-size: 12px;">
                <thead>
                  <tr>
                    <th style="width: 50%;font-size: 12px;">
                     Equipment
                    </th>
                    <th style="width: 10%;font-size: 12px; text-align: center;">Code</th>
                    <th style="width: 20%;"> </th>
                  </tr>
                </thead>
                <tbody>

                  <?php foreach ($eq as $rs) { ?>

                     <?php foreach ($rq_none as $rs_rq_n) { ?>

                      <tr class="r-eq" data-eq_id="<?php echo $rs['eq_id']; ?>" data-eq_name="<?php echo $rs['eq_name']; ?>" data-eq_code="<?php echo $rs['eq_code']; ?>">
                        <td>
                          <?php if(!empty($rs['eq_name'])){ echo $rs['eq_name']; } else { echo "-"; } ?>
                        </td>
                        <td style="width: 10%;font-size: 12px; text-align: center;"><?php echo $rs['eq_code']; ?></td>
                        <td class="td-actions">
                          <a href="<?php echo site_url(); ?>Other/MaintenanceDetail/<?php echo $rs['eq_id']; ?>" class="btn btn-small btn-success" title="Show"><i class="icon-eye-open icon-small"> </i></a>
                        </td>
                      </tr>

                      <?php } ?>

                  <?php } ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
</div>

  <!-- Add EQ -->
  <div class="modal fade" id="add-eq-type" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Equipment</h4>
        </div>
        <div class="modal-body">
              <label for="eq_name">Equipment Name:</label>
              <input type="text" class="form-control" id="eq_name" name="eq_name" style="width: 500px;">
              <label for="eq_code">Equipment Code:</label>
              <input type="text" class="form-control" id="eq_code" name="eq_code" style="width: 500px;">
              <p id="msg-error-eq" style="color: red;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success save-eq" >Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>

    <!-- Delete EQ -->
  <div class="modal fade" id="del-eq-type" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Equipment</h4>
        </div>
        <div class="modal-body">
              <p>Confirm to delete ?</p>
              <input type="hidden" id="del_eq_id" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success cf-del-eq" >Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>

  <!-- Add EQ -->
  <div class="modal fade" id="edit-eq-type" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Equipment</h4>
        </div>
        <div class="modal-body">
              <label for="eq_name">Equipment Name:</label>
              <input type="hidden" id="edit_eq_id">
              <input type="text" class="form-control" id="edit_eq_name" name="eq_name" style="width: 500px;">
              <label for="eq_code">Equipment Code:</label>
              <input type="text" class="form-control" id="edit_eq_code" name="eq_code" style="width: 500px;">
              <p id="msg-error-edit-eq" style="color: red;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success edit-eq" >Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>

<script src="<?php echo base_url(); ?>public/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript">
$(document).ready(function(){

  $('.save-eq').click(function(){
       
      var eq_name = $('#eq_name').val();
      var eq_code = $('#eq_code').val();

      if(eq_code == ''){
        $('#msg-error-eq').html('*Please Input Equipment Code');
      } else {
        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Setting/SaveEquipment',
          data:{ eq_name:eq_name, eq_code:eq_code }
        }).done(function(data){
            var o = JSON.parse(data);

            if(o.code_m == 'error'){
              $('#msg-error-eq').html(o.msg);
            }
            

            if(o.code_m == 'complete'){
              location.reload();
            }
        })

      }
         
  });

  $("table").off("click", ".del-eq-type");
  $("table").on("click", ".del-eq-type", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.r-eq');
      var eq_id = $row.data('eq_id');

      $('#del_eq_id').val(eq_id);
      
      $('#del-eq-type').modal('show');

  });

  $('.cf-del-eq').click(function(){
       
      var del_eq_id = $('#del_eq_id').val();

        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Setting/DelEquipment',
          data:{ del_eq_id:del_eq_id}
        }).done(function(data){
            var o = JSON.parse(data);
            alert(o.msg);

            if(o.code_m == 'complete'){
              location.reload();
            }
        })
         
  });

  $("table").off("click", ".edit-eq-type");
  $("table").on("click", ".edit-eq-type", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.r-eq');
      var eq_id = $row.data('eq_id');
      var eq_name = $row.data('eq_name');
      var eq_code = $row.data('eq_code');

      $('#edit_eq_id').val(eq_id);
      $('#edit_eq_name').val(eq_name);
      $('#edit_eq_code').val(eq_code);

      $('#edit-eq-type').modal('show');

  });

  $('.edit-eq').click(function(){
      
      var edit_eq_id = $('#edit_eq_id').val();
      var edit_eq_name = $('#edit_eq_name').val();
      var edit_eq_code = $('#edit_eq_code').val();

      if(eq_code == ''){
        $('#msg-error-edit-eq').html('*Please Input Equipment Code');
      } else {
        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Setting/EditEquipment',
          data:{ edit_eq_id:edit_eq_id, edit_eq_name:edit_eq_name, edit_eq_code:edit_eq_code }
        }).done(function(data){
            var o = JSON.parse(data);

            if(o.code_m == 'error'){
              $('#msg-error-edit-eq').html(o.msg);
            }
            
            if(o.code_m == 'complete'){
              location.reload();
            }
        })

      }
         
  });

});
</script>