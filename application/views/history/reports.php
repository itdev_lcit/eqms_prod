<style type="text/css">
  .label-danger {
    background-color: #db3325;
}
</style>
<div class="span12">
        <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Request History "<?php echo $eq_type->eq_name; ?>"</h3>
              <div align="right">

              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
             <table class="table table-striped table-bordered" style="font-size: 10px; width: 100%;" border="1">
                <thead>
                  <tr>
                    <th style="font-size: 12px;">
                     RQ
                    </th>
                    <th style="font-size: 12px; text-align: left;">Number</th>
                    <th style="font-size: 12px; text-align: left;">Priority</th>
                    <th style="font-size: 12px; text-align: left;"> Code</th>
                    <th style="font-size: 12px; text-align: left;"> Detail</th>
                    <th style="font-size: 12px; text-align: center;">RQ Status</th>
                    <th style="font-size: 12px; text-align: left;">Lost Time</th>
                    <th style="font-size: 12px; text-align: left;">Process Time</th>
                    <th style="font-size: 12px; text-align: left;">RQ Date</th>
                    <th style="font-size: 12px; text-align: left;">Accept Date</th>
                    <th style="font-size: 12px; text-align: left;">Complete Date</th>
                    <th style="font-size: 12px; text-align: left;">Engineer Remark</th>
                    <th style="font-size: 12px; text-align: left;">Support Remark</th>
                  </tr>
                </thead> 
                <tbody>

                <?php if($rq_order) { ?>
                      <?php $i=1; foreach ($rq_order as $rs) { ?>

                      <tr class="r-eq"  data-rq_id="<?php echo $rs['rq_id']; ?>">
                        <td>
                          <?php echo $rs['rq_id']; ?>
                        </td>
                        <td>
                          <?php echo $rs['eq_code'].$rs['eq_no']; ?>
                        </td>
                        <td>
                          <?php if($rs['rq_type'] == 'BD' ){ ?>
                              Breakdown
                            <?php } else if($rs['rq_type'] == 'PM' ) { ?>
                              Maintenance
                            <?php } else if($rs['rq_type'] == 'OT' ) { ?>
                              Other
                            <?php } ?>
                        </td>
                        <td>
                          <?php echo $rs['rq_code']; ?>
                        </td>
                        <td>
                          <?php echo $rs['rq_detail']; ?>
                        </td>
                        <td style="text-align: center;">
                            <?php if($rs['rq_accept'] == 'None' ){ ?>
                             <span style="color: gray;"><b><?php echo $rs['rq_accept']; ?></b></span>
                            <?php } else if($rs['rq_accept'] == 'ReadyRQ' ) { ?>
                              <span style="color: gray;"><b><?php echo $rs['rq_accept']; ?></b></span>
                            <?php } else if($rs['rq_accept'] == 'WaitRQ' ) { ?>
                              <span style="color: gray;"><b><?php echo $rs['rq_accept']; ?></b></span>
                            <?php } else if($rs['rq_accept'] == 'Process' ) { ?>
                              <span style="color: blue;"><b><?php echo $rs['rq_accept']; ?></b></span>
                            <?php } else if($rs['rq_accept'] == 'Complete' ) { ?>
                              <span style="color: green;"><b><?php echo $rs['rq_accept']; ?></b></span>
                            <?php } else if($rs['rq_accept'] == 'Wait' ) { ?>
                              <span style="color: orange;"><b><?php echo $rs['rq_accept']; ?></b></span>
                            <?php } ?>
                        </td>
                         <td>
                          <?php 

                              if($rs['complete_date'] != ''){

                                $date = new DateTime($rs['created']);
                                $now = new DateTime($rs['complete_date']);
                                echo $date->diff($now)->format("%d D: %h H: %i M");

                              } else {

                                $date = new DateTime($rs['created']);
                                $now = new DateTime();
                                echo $date->diff($now)->format("%d D: %h H: %i M");

                              }
                          ?>
                        </td>
                         <td>
                          <?php 

                            if($rs['accept_date'] != ''){

                              if($rs['complete_date'] != ''){

                                $date = new DateTime($rs['accept_date']);
                                $now = new DateTime($rs['complete_date']);
                                echo $date->diff($now)->format("%d D: %h H: %i M");

                              } else {

                                $date = new DateTime($rs['accept_date']);
                                $now = new DateTime();
                                echo $date->diff($now)->format("%d D: %h H: %i M");

                              }

                            } else {
                              echo '-';
                            }

                          ?>
                        </td>
                        <td>
                          <?php echo date("j-M-y H:i", strtotime($rs['created'])); ?>
                        </td>
                        <td>
                          <?php 
                            if($rs['accept_date'] != ''){
                              echo date("j-M-y H:i", strtotime($rs['accept_date'])); 
                            } else {
                              echo '-';
                            }
                          ?>
                        </td>
                        <td>
                          <?php 
                            if($rs['complete_date'] != ''){
                              echo date("j-M-y H:i", strtotime($rs['complete_date'])); 
                            } else {
                              echo '-';
                            }
                          ?>
                        </td>
                        <td>
                          <?php echo $rs['remark_w']; ?>
                        </td>
                        <td>
                          <?php echo $rs['remark_s']; ?>
                        </td>
                      </tr>

                      <?php $i++;  } ?>
                  <?php } else { ?>
                    <tr >
                      <td colspan="11" style="text-align: center;">-No Record-</td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
</div>
<script src="<?php echo base_url(); ?>public/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript">

$(document).ready(function(){
  window.print();
});
</script>