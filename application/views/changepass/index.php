<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>EQ-Maintenance</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
<link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>public/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url(); ?>public/css/font-awesome.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    
<link href="<?php echo base_url(); ?>public/css/style.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>public/css/pages/signin.css" rel="stylesheet" type="text/css">

</head>

<body>
    
    <div class="navbar navbar-fixed-top">
    
    <div class="navbar-inner">
        
        <div class="container">
            
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            
            <a class="brand" href="">
                Equipment Maintenance               
            </a>        

    
        </div> <!-- /container -->
        
    </div> <!-- /navbar-inner -->
    
</div> <!-- /navbar -->



<div class="account-container">
    
    <div class="content clearfix">
        
        <form id="FrmVat" role="form" action="" method="post" enctype="multipart/form-data">
        
            <h1>Change Password</h1>       
            
            <div class="login-fields">
                <p id="validate-password" style="color: red;"></p>
                 <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $user_id; ?>">
                    <div class="field">
                        <label for="password">New Password:</label>
                        <input type="password" id="password" name="password" value="" placeholder="New Password" class="login password-field"/>
                    </div> <!-- /password -->
                    
                    <div class="field">
                        <label for="password">Confirm Password:</label>
                        <input type="password" id="confirm_password" name="confirm_password" value="" placeholder="Confirm Password" class="login password-field"/>
                    </div> <!-- /password -->
            </div> <!-- /login-fields -->
            
            <div class="login-actions" align="right">
                <br>                
                <button type="button" class="btn btn-block btn-success save">Save</button>
                
            </div> <!-- .actions -->
 
        </form>
        
    </div> <!-- /content -->
    
</div> <!-- /account-container -->




<script src="<?php echo base_url(); ?>public/js/jquery-1.7.2.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/bootstrap.js"></script>

<script src="<?php echo base_url(); ?>public/js/signin.js"></script>

</body>

</html>

<!-- Modal -->
<div id="success" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div class="alert alert-success alert-dismissable">
                SUCCESS!!!
            </div>
          </div>
           <div class="modal-footer">
                <button type="submit" class="btn btn-success btn_succsess">OK</button>
            </div> 
        </div>

  </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('#navig-menu').css('display','none');


        $('.save').click(function(){

            var id = $('#user_id').val();
            var password = $('#password').val();
            var confirm_password = $('#confirm_password').val();

            if(password == ''){
                $('#validate-password').html('');
                $('#validate-password').append('Password Cannot be null.');
            } else if (confirm_password == '') {
                $('#validate-password').html('');
                $('#validate-password').append('Password Cannot be null.');
            } else {
                $.ajax({
                    url:'<?php echo site_url(); ?>ChangePass/SavePass',
                    type:'POST',
                    data:{ id:id, password:password, confirm_password:confirm_password}
                }).done(function(data){
                     var o = JSON.parse(data);

                    if (o.msg == 300){
                        $('#validate-password').html('');
                        $('#validate-password').append('Password Not Match.');
                    }

                    if(o.msg == 'success'){
                        $('#success').modal('show');
                    }
                    
                });
            }

        });

        $('.btn_succsess').click(function() {
            location.reload();
        });

    });
</script>
