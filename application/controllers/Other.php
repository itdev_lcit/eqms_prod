<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Other extends Welcome {

	public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->library("pagination");
        $this->load->model('M_Other');
    }


    public function Index(){

		if($this->session->userdata('logged_in')) { 	

			$this->db->where('is_del',0);
			$data['eq']  = $this->db->get('eq_type')->result_array();

			$this->db->where('is_del',0);
			$this->db->where('rq_accept','Process');
			$this->db->select('count(eq_id) as c_eq_id , eq_id');
			$data['rq_none']  = $this->db->get('rq_order')->result_array();

			$this->view['main'] =  $this->load->view('other/eq',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}


	public function MaintenanceDetail($eq_id=null , $rq_id = null){

		if($this->session->userdata('logged_in')) { 	

			$search = $this->input->post('type');
			$eq_no = $this->input->post('eq_no');
			$show = $this->input->post('show');

			$config = array();
	        $config["base_url"] = site_url() . "Other/MaintenanceDetail/".$eq_id;
	        $config["total_rows"] = $this->M_Other->countrq($search, $eq_id, $eq_no );

	       	$config["per_page"] = 15;
	        $config["uri_segment"] = 4;
	        $config['full_tag_open'] = '<div align="right"><ul class="pagination-revise" >';
	        $config['full_tag_close'] = '</ul></div><!--pagination-->';
	        $config['first_link'] = false;
	        $config['last_link'] = false;
	        $config['first_tag_open'] = '<li>';
	        $config['first_tag_close'] = '</li>';
	        $config['prev_link'] = 'Previous';
	        $config['prev_tag_open'] = '<li class="prev">';
	        $config['prev_tag_close'] = '</li>';
	        $config['next_link'] = 'Next';
	        $config['next_tag_open'] = '<li>';
	        $config['next_tag_close'] = '</li>';
	        $config['last_tag_open'] = '<li>';
	        $config['last_tag_close'] = '</li>';
	        $config['cur_tag_open'] = '<li class="active"><a href="#">';
	        $config['cur_tag_close'] = '</a></li>';
	        $config['num_tag_open'] = '<li>';
	        $config['num_tag_close'] = '</li>';	

	        $this->pagination->initialize($config);

	        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
	        $data["results"] = $this->M_Other->fetchRq($config["per_page"], $page , $search , $eq_id, $eq_no);
	        $data["links"] = $this->pagination->create_links();

			$data['eq_id'] = $eq_id;
			$data['eq_no'] = $eq_no;
			$data['type'] = $search;

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['eq_type']  = $this->db->get('eq_type')->row();


			$this->db->order_by('eq_no','asc');
			$this->db->where('is_del',0);
			$this->db->where('eq_type',$eq_id);
			$data['eq_detail']  = $this->db->get('eq_detail')->result_array();

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['man_service']  = $this->db->get('other_service')->result_array();

			$check_data = $this->session->userdata('logged_in');		
			$data['user'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();


			$this->view['main'] =  $this->load->view('other/index',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function SaveRequestOther(){

		if($this->session->userdata('logged_in')) { 

			$eq_id = $this->input->post('eq_id');
			$eq_no = $this->input->post('eq_no');
			$ms_system = $this->input->post('ms_system');

			$this->db->where('is_del',0);
			$this->db->where('ot_id',$ms_system);
			$MsDesc  = $this->db->get('other_service')->row();


			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();


			$eq = array(	
				"rq_type" => 'OT',
				"eq_id" => $eq_id,
				"eq_no" => $eq_no,
				"rq_code" => $MsDesc->ot_code,
				"rq_detail" => $MsDesc->ot_detail,
				"rq_accept" => 'None',
				"user_rq" => $userProfile->username,
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('rq_order', $eq);

			$bdStatus = array(	
				"eq_status" => 'Other',
				"updated" => date('Y-m-d H:i:s')
			);
			$this->db->where('eq_type',$eq_id);
			$this->db->where('eq_no',$eq_no);
			$msg = $this->db->update('eq_detail', $bdStatus);

			$log_process = array(	
				"detail" => json_encode($eq),
				"module" => 'Other/SaveRequestOther',
				"type_sql" => 'Insert',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function ChangeRequestStatus(){

		if($this->session->userdata('logged_in')) { 

			$rq_id = $this->input->post('rq_id');
			$rq_status = $this->input->post('rq_status');
			$wait_status = $this->input->post('wait_status');



			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->db->where('is_del',0);
			$this->db->where('rq_id',$rq_id);
			$Origin  = $this->db->get('rq_order')->row();


			if($rq_status == 'Process'){

				if($Origin->rq_accept == 'Wait'){
					$eq = array(
						"rq_accept" => $rq_status,
						"updated" => date('Y-m-d H:i:s')
					);
				} else if($Origin->accept_date != NULL) {
					$eq = array(
						"rq_accept" => $rq_status,
						"updated" => date('Y-m-d H:i:s')
					);
				} else {
					$eq = array(
						"rq_accept" => $rq_status,
						"accept_date" => date('Y-m-d H:i:s'),
						"updated" => date('Y-m-d H:i:s')
					);
				}

			} else if($rq_status == 'Wait'){

				$this->db->where('rq_id',$rq_id);
				$this->db->where('modify_status','Process');
				$CheckProcess2  = $this->db->get('log_work')->row();

				if(empty($CheckProcess2)){
					$result['msg'] = "Can't change status to 'Wait', This status could be 'Process' before.";
					echo json_encode($result);
					return false;
				}

				$eq = array(
					"remark_w" => $wait_status,
					"rq_accept" => $rq_status,
					"updated" => date('Y-m-d H:i:s')
				);
			} else if($rq_status == 'Complete'){

				$this->db->where('rq_id',$rq_id);
				$this->db->where('modify_status','Process');
				$CheckProcess2  = $this->db->get('log_work')->row();

				if(empty($CheckProcess2)){
					$result['msg'] = "Can't change status to 'Complete', This status could be 'Process' before.";
					echo json_encode($result);
					return false;
				}


				$this->db->where('is_del',0);
				$this->db->where('rq_id !=',$rq_id);
				$this->db->where('eq_id',$Origin->eq_id);
				$this->db->where('eq_no',$Origin->eq_no);
				//$this->db->where_in('rq_accept','Process','Wait','None');
				$this->db->where('(rq_accept= "None" OR rq_accept="Process" OR rq_accept="Wait")');
				$CheckRQ  = $this->db->get('rq_order')->row();

				if(empty($CheckRQ)){
					$bdStatus = array(	
						"eq_status" => 'Available',
						"updated" => date('Y-m-d H:i:s')
					);
					$this->db->where('eq_type',$Origin->eq_id);
					$this->db->where('eq_no',$Origin->eq_no);
					$msg = $this->db->update('eq_detail', $bdStatus);
				}

				$eq = array(
					"rq_accept" => $rq_status,
					"complete_date" => date('Y-m-d H:i:s'),
					"updated" => date('Y-m-d H:i:s')
				);
			}

			$this->db->where('rq_id',$rq_id);
			$msg = $this->db->update('rq_order', $eq);

			$log_process = array(	
				"detail" => json_encode($eq),
				"modify_detail" => json_encode($Origin),
				"module" => 'Maintenance/ChangeRequestStatus',
				"type_sql" => 'Update',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			$log_work = array(	
				"rq_id" => $rq_id,
				"origin_status" => $Origin->rq_accept,
				"modify_status" => $rq_status,
				"rq_type" => 'PM',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_work', $log_work);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function DelRequestMaintenance(){

		if($this->session->userdata('logged_in')) { 

			$rq_id = $this->input->post('del_rq_id');

			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->db->where('is_del',0);
			$this->db->where('rq_id',$rq_id);
			$OriginDetail =$this->db->get('rq_order')->row();


			$eq = array(
				"is_del" => 1,
				"updated" => date('Y-m-d H:i:s')
			);
			$this->db->where('rq_id',$rq_id);
			$msg = $this->db->update('rq_order', $eq);

				$this->db->where('is_del',0);
				$this->db->where('rq_id !=',$rq_id);
				$this->db->where('eq_id',$OriginDetail->eq_id);
				$this->db->where('eq_no',$OriginDetail->eq_no);
				$this->db->where_in('rq_accept','Process','Wait','None');
				$CheckRQ  = $this->db->get('rq_order')->row();


				if(empty($CheckRQ)){
					$bdStatus = array(	
						"eq_status" => 'Available',
						"updated" => date('Y-m-d H:i:s')
					);
					$this->db->where('eq_type',$OriginDetail->eq_id);
					$this->db->where('eq_no',$OriginDetail->eq_no);
					$msg = $this->db->update('eq_detail', $bdStatus);
				}
			
			$log_process = array(	
				"detail" => json_encode($eq),
				"modify_detail" => json_encode($OriginDetail),
				"module" => 'Breakdown/DelRequestBreakdown',
				"type_sql" => 'Update',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function History($eq_id=null , $eq_no = null){

		if($this->session->userdata('logged_in')) { 	

			$this->db->order_by('rq_order.created','DESC');
			$this->db->where('rq_order.is_del',0);
			$this->db->where('rq_order.eq_id',$eq_id);
			$this->db->where('rq_order.eq_no',$eq_no);
			$this->db->join('bd_detail','bd_detail.bdd_code = rq_order.rq_code');
			$this->db->join('eq_type','eq_type.eq_id = rq_order.eq_id');
			$this->db->select('bd_detail.bdd_code, bd_detail.bd_detail, eq_type.eq_name, eq_type.eq_code, rq_order.*');
			$data['rq_order']  = $this->db->get('rq_order')->result_array();

			$data['eq_id'] = $eq_id;

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['eq_type']  = $this->db->get('eq_type')->row();


			$this->db->order_by('eq_no','asc');
			$this->db->where('is_del',0);
			$this->db->where('eq_type',$eq_id);
			$data['eq_detail']  = $this->db->get('eq_detail')->result_array();

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['bd_system']  = $this->db->get('bd_system')->result_array();


			$this->view['main'] =  $this->load->view('breakdown/index',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function GetLogStatus(){

		$rq_id = $this->input->post('rq_id');

		if($rq_id != 0){

			$this->db->order_by('created','ASC');
			$this->db->where('rq_id',$rq_id);
			$rq_log  = $this->db->get('log_work')->result_array();

			echo json_encode($rq_log);
			return false;

		}

	}


	public function Detail($eq_id=null, $rq_id=null){

		if($this->session->userdata('logged_in')) { 	

			$this->db->where('rq_order.is_del',0);
			$this->db->where('rq_order.rq_type','OT');
			$this->db->where('other_service.is_del',0);
			$this->db->where('rq_order.rq_id',$rq_id);
			$this->db->join('other_service','other_service.ot_code = rq_order.rq_code');
			$this->db->join('eq_type','eq_type.eq_id = rq_order.eq_id');
			$this->db->select('other_service.ot_code, other_service.ot_detail, eq_type.eq_name, eq_type.eq_code, rq_order.*');
			$data['rq_order'] = $this->db->get('rq_order')->result_array();

			$data['eq_id'] = $eq_id;

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['eq_type']  = $this->db->get('eq_type')->row();

			$this->db->order_by('eq_no','asc');
			$this->db->where('is_del',0);
			$this->db->where('eq_type',$eq_id);
			$data['eq_detail']  = $this->db->get('eq_detail')->result_array();

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['bd_system']  = $this->db->get('bd_system')->result_array();

			$check_data = $this->session->userdata('logged_in');		
			$data['user'] = $this->db->get_where('user', array('id' => $check_data['id']))->row();


			$this->view['main'] =  $this->load->view('other/detail',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}


}
