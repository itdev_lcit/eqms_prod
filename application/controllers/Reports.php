<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Reports extends Welcome {

	public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->library("pagination");
    }


	public function Index($eq_id=null , $eq_no = null){

		if($this->session->userdata('logged_in')) { 

			$type = $this->input->post('type');
			$eq_no = $this->input->post('eq_no');
			$rq_code = $this->input->post('rq_code');
			$rq_status = $this->input->post('rq_status');
            $first_date = $this->input->post('first_date');
            $second_date = $this->input->post('second_date');	

            if(empty($first_date)){
				$data['first_date']  = date('Y-m-d');
				$cal_first_date = date('Y-m-d');
			} else {
				$data['first_date']  = $first_date;
				$cal_first_date = $first_date;
			}

			if(empty($second_date)){
				$data['second_date']  = date('Y-m-d');
				$cal_second_date = date('Y-m-d');
			} else {
				$data['second_date']  = $second_date;
				$cal_second_date = $second_date;
			}
 			
			$this->db->order_by('rq_order.eq_no','ASC');
			$this->db->where('rq_order.is_del',0);
			$this->db->join('eq_type','eq_type.eq_id = rq_order.eq_id');
			$this->db->select('eq_type.eq_name, eq_type.eq_code, rq_order.*');

			if($rq_code){
				$this->db->where('rq_order.rq_code',$rq_code);
			}

			if($type != 'All'){
				$this->db->where('rq_order.eq_id',$type);
			}

			if($eq_no != 'All' and !empty($eq_no)){
				$this->db->where('rq_order.rq_type',$eq_no);
			}

			if($rq_status != 'All' and !empty($rq_status)){
				$this->db->where('rq_order.rq_accept',$rq_status);
			}

			$this->db->where("rq_order.created BETWEEN '".$cal_first_date." 08:00"."' AND '".$cal_second_date." 07:59"."'");
			$data['rq_order']  = $this->db->get('rq_order')->result_array();

			$data['type']  = $type;
			$data['eq_no']  = $eq_no;
			$data['rq_status']  = $rq_status;
			$data['rq_code']  = $rq_code;

			$this->db->where('is_del',0);
			$data['eq_type']  = $this->db->get('eq_type')->result_array();


			$this->view['main'] =  $this->load->view('reports/index',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function PrintReport($rq_status=null , $type=null , $eq_no = null, $first_date = null, $second_date = null, $rq_code= null){


			if(empty($first_date)){
				$data['first_date']  = date('Y-m-d');
				$cal_first_date = date('Y-m-d');
			} else {
				$data['first_date']  = $first_date;
				$cal_first_date = $first_date;
			}

			if(empty($second_date)){
				$data['second_date']  = date('Y-m-d');
				$cal_second_date = date('Y-m-d');
			} else {
				$data['second_date']  = $second_date;
				$cal_second_date = $second_date;
			}

			$this->db->order_by('eq_type.eq_code','ASC');
			$this->db->order_by('rq_order.eq_no','ASC');
			$this->db->order_by('rq_order.created','ASC');

			$this->db->where('rq_order.is_del',0);
			$this->db->join('eq_type','eq_type.eq_id = rq_order.eq_id');
			$this->db->select('eq_type.eq_name, eq_type.eq_code, rq_order.*');
			
			if($rq_code){
				$this->db->where('rq_order.rq_code',$rq_code);
			}

			if($type != 'All'){
				$this->db->where('rq_order.eq_id',$type);
			}

			if($eq_no != 'All' and !empty($eq_no)){
				$this->db->where('rq_order.rq_type',$eq_no);
			}

			if($rq_status != 'All' and !empty($rq_status)){
				$this->db->where('rq_order.rq_accept',$rq_status);
			}

			$this->db->where("rq_order.created BETWEEN '".$cal_first_date." 08:00"."' AND '".$cal_second_date." 07:59"."'");
			$data['rq_order']  = $this->db->get('rq_order')->result_array();



			$this->load->view('reports/reports',$data);



	}

	public function GetEqNo(){

		$type = $this->input->post('type');

		if($type != 0){

			$this->db->order_by('eq_no','ASC');
			$this->db->where('eq_type',$type);
			$eq_detail  = $this->db->get('eq_detail')->result_array();

			echo json_encode($eq_detail);
			return false;

		}

	}

}
