<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Dashboard extends Welcome {

	public function index(){

		if($this->session->userdata('logged_in')) { 

			$eq_type = $this->input->post('eq_type');

			if(empty($eq_type) || $eq_type == '0'){
				$type = '1';
			} else {
				$type = $eq_type;
			}

			$data['type'] = $type;
			
			$this->db->where('is_del',0);
			$data['selectType']  = $this->db->get('eq_type')->result_array();

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$type);
			$data['eq_type']  = $this->db->get('eq_type')->result_array();

			$this->db->order_by('eq_no','asc');
			$this->db->where('is_del',0);
			$this->db->where('eq_type',$type);
			$this->db->select('eq_detail.* , (SELECT COUNT(*) FROM rq_order WHERE eq_no = eq_detail.eq_no AND eq_id = "'.$type.'" AND rq_accept != "Complete" AND is_del = "0") AS rq');
			$data['eq_detail']  = $this->db->get('eq_detail')->result_array();

			$this->db->where('is_del',0);
			$this->db->where('eq_status','Available');
			$this->db->where('eq_type',$type);
			$this->db->select('count(eq_name) as c_eq');
			$data['ava']  = $this->db->get('eq_detail')->row();

			$this->db->where('is_del',0);
			$this->db->where('eq_status','Maintenance');
			$this->db->where('eq_type',$type);
			$this->db->select('count(eq_name) as c_eq');
			$data['man']  = $this->db->get('eq_detail')->row();

			$this->db->where('is_del',0);
			$this->db->where('eq_status','Breakdown');
			$this->db->where('eq_type',$type);
			$this->db->select('count(eq_name) as c_eq');
			$data['bd']  = $this->db->get('eq_detail')->row();

			$this->db->where('is_del',0);
			$this->db->where('eq_status','Other');
			$this->db->where('eq_type',$type);
			$this->db->select('count(eq_name) as c_eq');
			$data['ot']  = $this->db->get('eq_detail')->row();

			//$this->db->limit(3);
			$this->db->order_by('rq_order.updated','desc');
			$this->db->where('rq_order.is_del',0);
			$this->db->where('(rq_order.rq_accept= "None" OR rq_order.rq_accept="ReadyRQ")');
			$this->db->join('eq_type','eq_type.eq_id = rq_order.eq_id');
			$this->db->select('eq_type.eq_name, eq_type.eq_code, rq_order.*');
			$data['rq_order']  = $this->db->get('rq_order')->result_array();


			$this->db->order_by('eq_type.order_num','asc');
			$typeEq= $this->db->get('eq_type')->result_array();

			foreach ($typeEq as $rs) {

				$this->db->where('eq_detail.eq_type',$rs['eq_id']);
				$this->db->where('eq_detail.is_del',0);
				$this->db->where('eq_type.is_del',0);
				$this->db->join('eq_type','eq_type.eq_id = eq_detail.eq_type');
				$this->db->select('eq_type.eq_id , eq_type.eq_name, eq_type.eq_code , sum(case when eq_detail.eq_status = "Available" then 1 else 0 end) as ava, sum(case when eq_detail.eq_status = "Maintenance" then 1 else 0 end) as man, sum(case when eq_detail.eq_status = "Breakdown" then 1 else 0 end) as bd , sum(case when eq_detail.eq_status = "Other" then 1 else 0 end) as ot , count(*) as total, eq_type.order_num');
				$summary[]  = $this->db->get('eq_detail')->row();

			}

			$data['summary'] = $summary;

			$this->view['main'] =  $this->load->view('dashboard/index',$data,true);
			$this->view();
		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}
}
