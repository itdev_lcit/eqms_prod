<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class History extends Welcome {

	public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->library("pagination");
    }


	public function Detail($eq_id=null , $eq_no = null){

		if($this->session->userdata('logged_in')) { 	

			$this->db->order_by('rq_order.created','DESC');
			$this->db->where('rq_order.eq_id',$eq_id);
			$this->db->where('rq_order.eq_no',$eq_no);
			$this->db->join('eq_type','eq_type.eq_id = rq_order.eq_id');
			$this->db->select('eq_type.eq_name, eq_type.eq_code, rq_order.*');
			$data['rq_order']  = $this->db->get('rq_order')->result_array();

			$data['eq_id'] = $eq_id;
			$data['eq_no'] = $eq_no;

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['eq_type']  = $this->db->get('eq_type')->row();


			$this->db->order_by('eq_no','asc');
			$this->db->where('is_del',0);
			$this->db->where('eq_type',$eq_id);
			$data['eq_detail']  = $this->db->get('eq_detail')->result_array();

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['bd_system']  = $this->db->get('bd_system')->result_array();


			$this->view['main'] =  $this->load->view('history/index',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function PrintReport($eq_id=null , $eq_no = null){

		if($this->session->userdata('logged_in')) { 	

			$this->db->order_by('rq_order.created','DESC');
			$this->db->where('rq_order.eq_id',$eq_id);
			$this->db->where('rq_order.eq_no',$eq_no);
			$this->db->join('eq_type','eq_type.eq_id = rq_order.eq_id');
			$this->db->select('eq_type.eq_name, eq_type.eq_code, rq_order.*');
			$data['rq_order']  = $this->db->get('rq_order')->result_array();

			$data['eq_id'] = $eq_id;
			$data['eq_no'] = $eq_no;

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['eq_type']  = $this->db->get('eq_type')->row();


			$this->db->order_by('eq_no','asc');
			$this->db->where('is_del',0);
			$this->db->where('eq_type',$eq_id);
			$data['eq_detail']  = $this->db->get('eq_detail')->result_array();

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['bd_system']  = $this->db->get('bd_system')->result_array();

			$this->load->view('history/reports',$data);

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

}
