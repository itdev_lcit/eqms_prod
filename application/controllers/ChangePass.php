<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class ChangePass extends Welcome {

	public function index(){
		if($this->session->userdata('logged_in')) { 	

			$check_data = $this->session->userdata('logged_in');	
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			if($username->is_change_pass == "0"){ 	

				$data['user_id'] =  $check_data['id'];
				$data['is_change_pass'] =  $username->is_change_pass;

				 $this->load->view('changepass/index',$data);


			} else {
				redirect('Dashboard');
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function SavePass(){

		$id = $this->input->post('id');
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');


			if($password != $confirm_password){
				$result['msg'] = "300";
				echo json_encode($result);
				return false;
			}

			$data = array(
          		"password" => md5($password),
          		"is_change_pass" => 1,
          		"updated" => date('Y-m-d H:i:s')
	          );
	         
	        $this->db->where('id', $id);
			$this->db->update('user',$data);

			$result['msg'] = "success";
			echo json_encode($result);
			return false;


	}

}