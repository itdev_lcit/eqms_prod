<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Setting extends Welcome {

	public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->library("pagination");
    }

	public function Equipment(){

		if($this->session->userdata('logged_in')) { 	

			$this->db->where('is_del',0);
			$data['eq']  = $this->db->get('eq_type')->result_array();

			$this->view['main'] =  $this->load->view('setting/equipment/eq',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function SaveEquipment(){

		if($this->session->userdata('logged_in')) { 

			$eq_name = $this->input->post('eq_name');
			$eq_code = $this->input->post('eq_code');


			/*if($eq_name != null){
				$this->db->where('is_del',0);
				$this->db->like('eq_name', $eq_name);
				$checkEqName =$this->db->get('eq_type')->row();

				if($checkEqName){

					$result['msg'] = "Invalid Equipment Name.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}*/

			if($eq_code != null){
				$this->db->where('is_del',0);
				$this->db->like('eq_code', $eq_code);
				$checkEqCode =$this->db->get('eq_type')->row();

				if($checkEqCode){

					$result['msg'] = "Invalid Equipment Code.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}

			$eq = array(	
				"eq_name" => $eq_name,
				"eq_code" => $eq_code,
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('eq_type', $eq);

			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$log_process = array(	
				"detail" => json_encode($eq),
				"module" => 'Setting/SaveEquipment',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"type_sql" => 'Insert',
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function DelEquipment(){

		if($this->session->userdata('logged_in')) { 

			$eq_id = $this->input->post('del_eq_id');

			$data = array(
				"is_del" => 1,
				"updated" => date('Y-m-d H:i:s')
			);

			$this->db->where('eq_id', $eq_id);
			$this->db->update('eq_type',$data);

			$this->db->where('eq_id',$eq_id);
			$eq  = $this->db->get('eq_type')->result_array();

			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();

				$log_process = array(	
					"detail" => json_encode($data),
					"modify_detail" => json_encode($eq),
					"module" => 'Setting/DelEquipment',
					"type_sql" => 'Update',
					"user" => $userProfile->username,
					"name" => $userProfile->name,
					"created" => date('Y-m-d H:i:s')
				);

				$msg = $this->db->insert('log_process', $log_process);

			$result['msg'] = "Success";
			$result['code_m'] = "complete";
			echo json_encode($result);
			return false;

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function EditEquipment(){

		if($this->session->userdata('logged_in')) { 

			$edit_eq_id = $this->input->post('edit_eq_id');
			$edit_eq_name = $this->input->post('edit_eq_name');
			$edit_eq_code = $this->input->post('edit_eq_code');

			$this->db->where('is_del',0);
			$this->db->like('eq_id', $edit_eq_id);
			$checkEqType =$this->db->get('eq_type')->row();

			if($checkEqType->eq_name != $edit_eq_name){

				$this->db->where('is_del',0);
				$this->db->where('eq_id !=',$edit_eq_id);
				$this->db->like('eq_name', $edit_eq_name);
				$checkEqName =$this->db->get('eq_type')->row();

				if($checkEqName){

					$result['msg'] = "Invalid Equipment Name.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;
				}

			}

			if($checkEqType->eq_name != $edit_eq_name){

				$this->db->where('is_del',0);
				$this->db->where('eq_id !=',$edit_eq_id);
				$this->db->like('eq_code', $edit_eq_code);
				$checkEqName =$this->db->get('eq_type')->row();

				if($checkEqName){
					
					$result['msg'] = "Invalid Equipment Code.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;
				}

			}

			$eq = array(	
				"eq_name" => $edit_eq_name,
				"eq_code" => $edit_eq_code,
				"updated" => date('Y-m-d H:i:s')
			);

			$this->db->where('eq_id', $edit_eq_id);
			$this->db->update('eq_type',$eq);

			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$log_process = array(	
				"detail" => json_encode($eq),
				"modify_detail" => json_encode($checkEqType),
				"module" => 'Setting/EditEquipment',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"type_sql" => 'Update',
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function EquipmentDetail($eq_id = null){

		if($this->session->userdata('logged_in')) { 	

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['eq']  = $this->db->get('eq_type')->result_array();

			$this->db->order_by('eq_no','asc');
			$this->db->where('is_del',0);
			$this->db->where('eq_type',$eq_id);
			$data['eq_detail']  = $this->db->get('eq_detail')->result_array();

			$this->view['main'] =  $this->load->view('setting/equipment/eq_detail',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function SaveEquipmentNumber(){

		if($this->session->userdata('logged_in')) { 

			$eq_no = $this->input->post('eq_no');
			$eq_type = $this->input->post('eq_type');


			if($eq_no != null){

				$this->db->where('is_del',0);
				$this->db->where('eq_type',$eq_type);
				$this->db->like('eq_no', $eq_no);
				$checkEqNumber =$this->db->get('eq_detail')->row();

				if($checkEqNumber){

					$result['msg'] = "Invalid Equipment Number.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}


			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			

			$this->db->where('eq_id',$eq_type);
			$this->db->where('is_del', 0);
			$this->db->select('eq_count,updated,eq_code');
			$checkEqCount =$this->db->get('eq_type')->row();

			$eqCount = array(	
				"eq_count" => $checkEqCount->eq_count+1,
				"updated" => date('Y-m-d H:i:s')
			);

			$this->db->where('eq_id', $eq_type);
			$this->db->where('is_del', 0);
			$this->db->update('eq_type',$eqCount);

			$log_process = array(	
				"detail" => json_encode($eqCount),
				"modify_detail" => json_encode($checkEqCount),
				"module" => 'Setting/SaveEquipmentNumber',
				"type_sql" => 'Update',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			$eq = array(	
				"eq_no" => $eq_no,
				"eq_type" => $eq_type,
				"eq_name" => $checkEqCount->eq_code,
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('eq_detail', $eq);


			

			$log_process = array(	
				"detail" => json_encode($eq),
				"module" => 'Setting/SaveEquipmentNumber',
				"type_sql" => 'Insert',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function DelEquipmentDetail(){

		if($this->session->userdata('logged_in')) { 

			$eqd_id = $this->input->post('del_eqd_id');
			$eq_type = $this->input->post('del_eqd_type');

			$this->db->where('eqd_id',$eqd_id);
			$eq  = $this->db->get('eq_detail')->result_array();

			$data = array(
				"is_del" => 1,
				"updated" => date('Y-m-d H:i:s')
			);

			$this->db->where('eqd_id', $eqd_id);
			$this->db->update('eq_detail',$data);

			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();

				$log_process = array(	
					"detail" => json_encode($data),
					"modify_detail" => json_encode($eq),
					"module" => 'Setting/DelEquipmentDetail',
					"type_sql" => 'Update',
					"user" => $userProfile->username,
					"name" => $userProfile->name,
					"created" => date('Y-m-d H:i:s')
				);

				$msg = $this->db->insert('log_process', $log_process);


			$this->db->where('eq_id',$eq_type);
			$this->db->where('is_del', 0);
			$this->db->select('eq_count,updated');
			$checkEqCount =$this->db->get('eq_type')->row();

			$eqCount = array(	
				"eq_count" => $checkEqCount->eq_count-1,
				"updated" => date('Y-m-d H:i:s')
			);

			$this->db->where('eq_id', $eq_type);
			$this->db->where('is_del', 0);
			$this->db->update('eq_type',$eqCount);

			$log_process = array(	
				"detail" => json_encode($eqCount),
				"modify_detail" => json_encode($checkEqCount),
				"module" => 'Setting/DelEquipmentDetail',
				"type_sql" => 'Update',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$this->db->insert('log_process', $log_process);

			$result['msg'] = "Success";
			$result['code_m'] = "complete";
			echo json_encode($result);
			return false;

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function Breakdown(){

		if($this->session->userdata('logged_in')) { 	

			$this->db->where('is_del',0);
			$data['bd']  = $this->db->get('eq_type')->result_array();

			$this->view['main'] =  $this->load->view('setting/breakdown/bd',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function BreakdownDetail($eq_id = null){

		if($this->session->userdata('logged_in')) { 	

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['bd']  = $this->db->get('eq_type')->result_array();


			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['eq_detail']  = $this->db->get('bd_system')->result_array();

			$this->view['main'] =  $this->load->view('setting/breakdown/bd_detail',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function SaveBreakdownSystem(){

		if($this->session->userdata('logged_in')) { 

			$bd_name = $this->input->post('bd_name');
			$bd_code = $this->input->post('bd_code');
			$eq_type = $this->input->post('eq_type');


			if($bd_name != null){

				$this->db->where('is_del',0);
				$this->db->where('eq_id',$eq_type);
				$this->db->like('bd_name', $bd_name);
				$checkEqNumber =$this->db->get('bd_system')->row();

				if($checkEqNumber){

					$result['msg'] = "Invalid System Name.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}

			if($bd_code != null){

				$this->db->where('is_del',0);
				$this->db->where('eq_id',$eq_type);
				$this->db->like('bd_name', $bd_name);
				$this->db->like('bd_code', $bd_code);
				$checkEqNumber =$this->db->get('bd_system')->row();

				if($checkEqNumber){

					$result['msg'] = "Invalid System Code.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}


			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();



			$eq = array(	
				"bd_name" => $bd_name,
				"eq_id" => $eq_type,
				"bd_code" => $bd_code,
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('bd_system', $eq);

			$log_process = array(	
				"detail" => json_encode($eq),
				"module" => 'Setting/SaveBreakdownSystem',
				"type_sql" => 'Insert',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function EditBreakdownSystem(){

		if($this->session->userdata('logged_in')) { 

			$bd_name = $this->input->post('bd_name');
			$bd_code = $this->input->post('bd_code');
			$bd_id = $this->input->post('bd_id');


			if($bd_name != null){

				$this->db->where('is_del',0);
				$this->db->where('bd_id !=',$bd_id);
				$this->db->like('bd_name', $bd_name);
				$checkEqNumber =$this->db->get('bd_system')->row();

				if($checkEqNumber){

					$result['msg'] = "Invalid System Name.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}

			if($bd_code != null){

				$this->db->where('is_del',0);
				$this->db->where('bd_id !=',$bd_id);
				$this->db->like('bd_code', $bd_code);
				$checkEqNumber =$this->db->get('bd_system')->row();

				if($checkEqNumber){

					$result['msg'] = "Invalid System Code.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}


			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->db->where('is_del',0);
			$this->db->where('bd_id',$bd_id);
			$this->db->select('bd_id, bd_name, bd_code, updated');
			$OriginDetail =$this->db->get('bd_system')->row();


			$eq = array(	
				"bd_name" => $bd_name,
				"bd_code" => $bd_code,
				"updated" => date('Y-m-d H:i:s')
			);
			$this->db->where('bd_id',$bd_id);
			$msg = $this->db->update('bd_system', $eq);
			
			$log_process = array(	
				"detail" => json_encode($eq),
				"modify_detail" => json_encode($OriginDetail),
				"module" => 'Setting/EditBreakdownSystem',
				"type_sql" => 'Update',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function DelBreakdownSystem(){

		if($this->session->userdata('logged_in')) { 

			$bd_id = $this->input->post('bd_id');

			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->db->where('is_del',0);
			$this->db->where('bd_id',$bd_id);
			$this->db->select('bd_id, is_del, updated');
			$OriginDetail =$this->db->get('bd_system')->row();


			$eq = array(
				"is_del" => 1,
				"updated" => date('Y-m-d H:i:s')
			);
			$this->db->where('bd_id',$bd_id);
			$msg = $this->db->update('bd_system', $eq);
			
			$log_process = array(	
				"detail" => json_encode($eq),
				"modify_detail" => json_encode($OriginDetail),
				"module" => 'Setting/DelBreakdownSystem',
				"type_sql" => 'Update',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function BreakdownSystemDetail($bd_id = null){

		if($this->session->userdata('logged_in')) { 	

			$this->db->where('is_del',0);
			$this->db->where('bd_id',$bd_id);
			$data['bd_system']  = $this->db->get('bd_system')->row();

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$data['bd_system']->eq_id);
			$data['bd']  = $this->db->get('eq_type')->row();

			$this->db->where('is_del',0);
			$this->db->where('bd_id',$bd_id);
			$data['bd_detail']  = $this->db->get('bd_detail')->result_array();

			$this->view['main'] =  $this->load->view('setting/breakdown/bd_system_detail',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function SaveBreakdownSystemDetail(){

		if($this->session->userdata('logged_in')) { 

			$bd_id = $this->input->post('bd_id');
			$bdd_code = $this->input->post('bdd_code');
			$bd_detail = $this->input->post('bd_detail');


			if($bdd_code != null){

				$this->db->where('is_del',0);
				$this->db->where('bd_id',$bd_id);
				$this->db->like('bdd_code', $bdd_code);
				$checkEqNumber =$this->db->get('bd_detail')->row();

				if($checkEqNumber){

					$result['msg'] = "Invalid System Detail Code.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}

			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();


			$eq = array(	
				"bd_id" => $bd_id,
				"bdd_code" => $bdd_code,
				"bd_detail" => $bd_detail,
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('bd_detail', $eq);

			$log_process = array(	
				"detail" => json_encode($eq),
				"module" => 'Setting/SaveBreakdownSystemDetail',
				"type_sql" => 'Insert',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function EditBreakdownSystemDetail(){

		if($this->session->userdata('logged_in')) { 

			$bdd_id = $this->input->post('bdd_id');
			$bdd_code = $this->input->post('bdd_code');
			$bd_detail = $this->input->post('bd_detail');


			if($bdd_code != null){

				$this->db->where('is_del',0);
				$this->db->where('bdd_id !=',$bdd_id);
				$this->db->like('bdd_code', $bdd_code);
				$checkEqNumber =$this->db->get('bd_detail')->row();

				if($checkEqNumber){

					$result['msg'] = "Invalid System Detail Code.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}

			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->db->where('is_del',0);
			$this->db->where('bdd_id !=',$bdd_id);
			$this->db->like('bdd_code', $bdd_code);
			$Origin =$this->db->get('bd_detail')->row();

			$eq = array(
				"bdd_code" => $bdd_code,
				"bd_detail" => $bd_detail,
				"updated" => date('Y-m-d H:i:s')
			);
			$this->db->where('bdd_id', $bdd_id);
			$msg = $this->db->update('bd_detail', $eq);

			$log_process = array(	
				"detail" => json_encode($eq),
				"modify_detail" => json_encode($Origin),
				"module" => 'Setting/EditBreakdownSystemDetail',
				"type_sql" => 'Update',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}


	public function DelBreakdownSystemDetail(){

		if($this->session->userdata('logged_in')) { 

			$bdd_id = $this->input->post('bdd_id');


			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->db->where('is_del',0);
			$this->db->where('bdd_id',$bdd_id);
			$this->db->select('bdd_id, is_del, updated, bdd_id');
			$OriginDetail =$this->db->get('bd_detail')->row();


			$eq = array(
				"is_del" => 1,
				"updated" => date('Y-m-d H:i:s')
			);
			$this->db->where('bdd_id',$bdd_id);
			$msg = $this->db->update('bd_detail', $eq);
			
			$log_process = array(	
				"detail" => json_encode($eq),
				"modify_detail" => json_encode($OriginDetail),
				"module" => 'Setting/DelBreakdownSystemDetail',
				"type_sql" => 'Update',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function Maintenance(){

		if($this->session->userdata('logged_in')) { 	

			$this->db->where('is_del',0);
			$data['bd']  = $this->db->get('eq_type')->result_array();

			$this->view['main'] =  $this->load->view('setting/maintenance/index',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function MaintenanceService($eq_id = null){

		if($this->session->userdata('logged_in')) { 	

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['bd']  = $this->db->get('eq_type')->row();


			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['service']  = $this->db->get('man_service')->result_array();

			$this->view['main'] =  $this->load->view('setting/maintenance/service',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function SaveMaintenanceService(){

		if($this->session->userdata('logged_in')) { 

			$eq_id = $this->input->post('eq_id');
			$ms_name = $this->input->post('ms_name');
			$ms_code = $this->input->post('ms_code');
			$ms_detail = $this->input->post('ms_detail');


			if($ms_code != null){

				$this->db->where('is_del',0);
				$this->db->where('eq_id',$eq_id);
				$this->db->like('ms_code', $ms_code);
				$checkEqNumber =$this->db->get('man_service')->row();

				if($checkEqNumber){

					$result['msg'] = "Invalid Service Code.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}

			if($ms_name != null){

				$this->db->where('is_del',0);
				$this->db->where('eq_id',$eq_id);
				$this->db->like('ms_name', $ms_name);
				$checkEqNumber =$this->db->get('man_service')->row();

				if($checkEqNumber){

					$result['msg'] = "Invalid Service Name.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}


			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();


			$eq = array(	
				"eq_id" => $eq_id,
				"ms_name" => $ms_name,
				"ms_code" => $ms_code,
				"ms_detail" => $ms_detail,
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('man_service', $eq);

			$log_process = array(	
				"detail" => json_encode($eq),
				"module" => 'Setting/SaveMaintenanceService',
				"type_sql" => 'Insert',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function EditMaintenanceService(){

		if($this->session->userdata('logged_in')) { 

			$ms_id = $this->input->post('ms_id');
			$eq_id = $this->input->post('eq_id');
			$ms_name = $this->input->post('ms_name');
			$ms_code = $this->input->post('ms_code');
			$ms_detail = $this->input->post('ms_detail');


			if($ms_code != null){

				$this->db->where('is_del',0);
				$this->db->where('eq_id',$eq_id);
				$this->db->where('ms_id !=',$ms_id);
				$this->db->like('ms_code', $ms_code);
				$checkEqNumber =$this->db->get('man_service')->row();

				if($checkEqNumber){

					$result['msg'] = "Invalid Service Code.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}

			if($ms_name != null){

				$this->db->where('is_del',0);
				$this->db->where('eq_id',$eq_id);
				$this->db->where('ms_id !=',$ms_id);
				$this->db->like('ms_name', $ms_name);
				$checkEqNumber =$this->db->get('man_service')->row();

				if($checkEqNumber){

					$result['msg'] = "Invalid Service Name.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}


			$this->db->where('ms_id',$ms_id);
			$Origin =$this->db->get('man_service')->row();

			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();


			$eq = array(	
				"ms_name" => $ms_name,
				"ms_code" => $ms_code,
				"ms_detail" => $ms_detail,
				"updated" => date('Y-m-d H:i:s')
			);
			$this->db->where('ms_id',$ms_id);
			$msg = $this->db->update('man_service', $eq);

			$log_process = array(	
				"detail" => json_encode($eq),
				"modify_detail" => json_encode($Origin),
				"module" => 'Setting/EditMaintenanceService',
				"type_sql" => 'Update',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function DelMaintenanceService(){

		if($this->session->userdata('logged_in')) { 

			$ms_id = $this->input->post('ms_id');


			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->db->where('is_del',0);
			$this->db->where('ms_id',$ms_id);
			$this->db->select('ms_id, is_del, updated, eq_id');
			$OriginDetail =$this->db->get('man_service')->row();


			$eq = array(
				"is_del" => 1,
				"updated" => date('Y-m-d H:i:s')
			);
			$this->db->where('ms_id',$ms_id);
			$msg = $this->db->update('man_service', $eq);
			
			$log_process = array(	
				"detail" => json_encode($eq),
				"modify_detail" => json_encode($OriginDetail),
				"module" => 'Setting/DelMaintenanceService',
				"type_sql" => 'Update',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function Other(){

		if($this->session->userdata('logged_in')) { 	

			$this->db->where('is_del',0);
			$data['bd']  = $this->db->get('eq_type')->result_array();

			$this->view['main'] =  $this->load->view('setting/other/index',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function OtherService($eq_id = null){

		if($this->session->userdata('logged_in')) { 	

			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['bd']  = $this->db->get('eq_type')->row();


			$this->db->where('is_del',0);
			$this->db->where('eq_id',$eq_id);
			$data['service']  = $this->db->get('other_service')->result_array();

			$this->view['main'] =  $this->load->view('setting/other/service',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function SaveOtherService(){

		if($this->session->userdata('logged_in')) { 

			$eq_id = $this->input->post('eq_id');
			$ot_name = $this->input->post('ms_name');
			$ot_code = $this->input->post('ms_code');
			$ot_detail = $this->input->post('ms_detail');


			if($ot_code != null){

				$this->db->where('is_del',0);
				$this->db->where('eq_id',$eq_id);
				$this->db->like('ot_code', $ot_code);
				$checkEqNumber =$this->db->get('other_service')->row();

				if($checkEqNumber){

					$result['msg'] = "Invalid Service Code.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}

			if($ot_name != null){

				$this->db->where('is_del',0);
				$this->db->where('eq_id',$eq_id);
				$this->db->like('ot_name', $ot_name);
				$checkEqNumber =$this->db->get('other_service')->row();

				if($checkEqNumber){

					$result['msg'] = "Invalid Service Name.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}


			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();


			$eq = array(	
				"eq_id" => $eq_id,
				"ot_name" => $ot_name,
				"ot_code" => $ot_code,
				"ot_detail" => $ot_detail,
				"created" => date('Y-m-d H:i:s'),
				"updated" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('other_service', $eq);

			$log_process = array(	
				"detail" => json_encode($eq),
				"module" => 'Setting/SaveOtherService',
				"type_sql" => 'Insert',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function EditOtherService(){

		if($this->session->userdata('logged_in')) { 

			$ot_id = $this->input->post('ms_id');
			$eq_id = $this->input->post('eq_id');
			$ot_name = $this->input->post('ms_name');
			$ot_code = $this->input->post('ms_code');
			$ot_detail = $this->input->post('ms_detail');


			if($ot_code != null){

				$this->db->where('is_del',0);
				$this->db->where('eq_id',$eq_id);
				$this->db->where('ot_id !=',$ot_id);
				$this->db->like('ot_code', $ot_code);
				$checkEqNumber =$this->db->get('other_service')->row();

				if($checkEqNumber){

					$result['msg'] = "Invalid Service Code.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}

			if($ot_name != null){

				$this->db->where('is_del',0);
				$this->db->where('eq_id',$eq_id);
				$this->db->where('ot_id !=',$ot_id);
				$this->db->like('ot_name', $ot_name);
				$checkEqNumber =$this->db->get('other_service')->row();

				if($checkEqNumber){

					$result['msg'] = "Invalid Service Name.";
					$result['code_m'] = "error";
					echo json_encode($result);
					return false;

				}
			}


			$this->db->where('ot_id',$ot_id);
			$Origin =$this->db->get('other_service')->row();

			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();


			$eq = array(	
				"ot_name" => $ot_name,
				"ot_code" => $ot_code,
				"ot_detail" => $ot_detail,
				"updated" => date('Y-m-d H:i:s')
			);
			$this->db->where('ot_id',$ot_id);
			$msg = $this->db->update('other_service', $eq);

			$log_process = array(	
				"detail" => json_encode($eq),
				"modify_detail" => json_encode($Origin),
				"module" => 'Setting/EditOtherService',
				"type_sql" => 'Update',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	public function DelOtherService(){

		if($this->session->userdata('logged_in')) { 

			$ot_id = $this->input->post('ms_id');


			$check_data = $this->session->userdata('logged_in');		
			$userProfile = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			$this->db->where('is_del',0);
			$this->db->where('ot_id',$ot_id);
			$this->db->select('ot_id, is_del, updated, eq_id');
			$OriginDetail =$this->db->get('other_service')->row();


			$eq = array(
				"is_del" => 1,
				"updated" => date('Y-m-d H:i:s')
			);
			$this->db->where('ot_id',$ot_id);
			$msg = $this->db->update('other_service', $eq);
			
			$log_process = array(	
				"detail" => json_encode($eq),
				"modify_detail" => json_encode($OriginDetail),
				"module" => 'Setting/DelOtherService',
				"type_sql" => 'Update',
				"user" => $userProfile->username,
				"name" => $userProfile->name,
				"created" => date('Y-m-d H:i:s')
			);

			$msg = $this->db->insert('log_process', $log_process);

			if($msg){
				$result['msg'] = "Success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
	}

	
}
